<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
    <jsp:include page="../template/navbar.jsp"/>            

        <div class="container mt-4" >

                <h1>Factura Emitidas</h1>
                
                <br>                
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>N° Factura</th>
                        <th>Precio</th>
                        <th>Rut Cliente</th>
                    </tr>
                </thead>
                <c:forEach items="${lista}" var="p" >
                    <tbody> 
                        <tr>
                            <td><input type="text" value="${p.numeroFactura}"/></td>
                            <td><input type="text" value="${p.precioFactura}"/></td>
                            <td><input type="text" value="${p.clientesRutCliente.rutCliente}"/></td>
                        </tr>   
                    </tbody>
                </c:forEach>
            </table>
				   							
            <div class="form-group">
                <div class="form-group mx-sm-3">
                    <button type="button" class="btn btn-primary">Imprimir</button>
                </div>
             </div>

        </div>
    </body>
</html>