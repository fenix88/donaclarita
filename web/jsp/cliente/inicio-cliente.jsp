<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
    </head>
    <body>

        <jsp:include page="../template/navbar.jsp"/>



        <div class="container mt-4" >

            <div class="row row-cols-1 row-cols-md-4">
                <div class="col mb-3">
                    <div class="card h-100">
                        <img  src="${pageContext.request.contextPath}/img/registrar_cliente.png" height="180" width="180" style="margin-left: 35px; margin-right: 25px; margin-top: 25px;">
                        <div class="card-body">
                            <br>
                            <h5 class="card-title" style="text-align: center;" >Mi perfil</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/jsp/cliente/mantenedor-clientes.jsp" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="card h-100">
                        <img  src="${pageContext.request.contextPath}/img/mantenedor_habitaciones.png" height="180" width="180" style="margin-left: 35px; margin-right: 25px; margin-top: 25px;">
                        <div class="card-body">
                            <br>
                            <h5 class="card-title" style="text-align: center;">Listar Habitaciones</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/listar-habitaciones-servlet" class="btn btn-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="card h-100">
                        <img  src="${pageContext.request.contextPath}/img/servicio-comedor.jpg" height="180" width="180" style="margin-left: 35px; margin-right: 25px; margin-top: 25px;" >
                        <div class="card-body">
                            <br>
                            <h5 class="card-title" style="text-align: center;">Servicio Comedor</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/listar-servicio" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="card h-100">
                        <img  src="${pageContext.request.contextPath}/img/factura.jpg" height="180" width="180" style="margin-left: 35px; margin-right: 25px; margin-top: 25px;">
                        <div class="card-body">
                            <br>
                            <h5 class="card-title" style="text-align: center;">Factura</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/factura-servlet" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" style="width: 16rem;">
                <div class="card h-100">
                    <img  src="${pageContext.request.contextPath}/img/mantenedor-orden-compra.jpg" height="180" width="180" style="margin-left: 35px; margin-right: 25px; margin-top: 25px;">
                    <div class="card-body">
                        <br>
                        <h5 class="card-title" style="text-align: center;">Orden de Compra</h5>
                        <p class="card-text"></p>
                        <a href="${pageContext.request.contextPath}/orden-compra" class="btn btn-outline-primary btn-block" style="margin-left: 15px;" > Entrar </a>
                    </div>
                </div>
            </div>



    </body>
</html>
