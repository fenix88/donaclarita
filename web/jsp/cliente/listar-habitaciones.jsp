<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
       
        <div class="container" >
             <h1>Listado de Habitaciones</h1>
            <div class="row row-cols-1 row-cols-md-4">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>N° habitacion</th>
                            <th>Tipo Cama</th>
                            <th>Accesorio</th>
                            <th>Precio</th>
                            <th>Cantidad Accesorios</th>                          
                          <!--  <th>Estado Habitacion</th> -->
                        </tr>
                    </thead>
                    <c:forEach items="${lista}" var="p" >
                        <tbody>
                            <tr>
                                <td><input type="text" value="${p.numeroHabitacion}"/></td>
                                <td><input type="text" value="${p.tipoCama}"/></td>
                                <td><input type="text" value="${p.accesorio}"/></td>
                                <td><input type="text" value="${p.precio}"/></td>
                                <td><input type="text" value="${p.cantidadAccesorios}"/></td>                              
                            </tr>
                        </tbody>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
</html>
