<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            

        <div class="container mt-4" >

            <h1>Modulo Orden de Compra</h1>

            <form>

                <br> 

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">N° de orden:</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-numero-orden" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Cliente</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-rut-cliente" placeholder="" >
                    </div>
                </div>

                <div class="form-group row">

                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-rut-huesped" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="nombre-huesped" placeholder="" >
                    </div>
                </div> 

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Apellido Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="apellido-huesdped" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">N° Habitacion</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="numero-habitacion" placeholder="" >
                    </div>
                </div>

                <br>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="button" class="btn btn-success">Agregar Orden</button>
                    </div>
                </div>

            </form>
        </div>
        <section>
            <br>
            <h3 style="text-align: center;">Listado Proveedores</h3>

            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>N°de Orden</th>
                        <th>Rut Cliente</th>
                        <th>Rut Huesped</th>
                        <th>Nombre Huesped</th>
                        <th>Apellido Huesped </th>
                        <th>N° Habitacion</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <c:forEach items="${lista}" var="p" varStatus="i" >
                    <tbody> 
                        <tr>
                            <td><input type="text" id="txt-numero-orden-${i.count}" value="${p.ordenesCompraNumeroOrden.numeroOrden}" disabled="true" style="font-size: 10px !important" /></td>
                            <td><input type="text" id="txt-rut-cliente-${i.count}" value="${p.clientesRutCliente.rutCliente}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-rut-huesped-${i.count}" value="${p.rutHuesped}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-nombre-huesped-${i.count}" value="${p.nombreHuesped}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-apellido-huesped-${i.count}" value="${p.apellidoHuesped}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-numero-habitacion-${i.count}" value="${p.habitacionesIdHabitacion.txt_rut_cliente}" disabled="true" style="font-size: 10px !important"/></td>
                            <td>
                                <div class="form-inline">
                                    <button id="bnt-edit-${i.count}" type="button" onclick="edit('${i.count}')" class="btn btn-warning btn-sm">Modificar</button> 
                                    <button id="bnt-delete-${i.count}" type="button" onclick="deleteRow('${i.count}')" class="btn btn-danger btn-sm ">Eliminar</button>  
                                    <button id="bnt-save-${i.count}" type="button" onclick="saveRow('${i.count}')" class="btn btn-success btn-sm" hidden="true">Guardar</button>
                                    <button id="bnt-cancel-${i.count}" type="button" onclick="cancelRow('${i.count}')" class="btn btn-danger btn-sm" hidden="true">CANCEL</button>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                </c:forEach>
            </table>
        </section>
    </body>
    
    <script>
        
         function edit(row) {


            editRow(row, false, false, false, false, false, false, true, true,
                    false, false);
        }

        function cancelRow(row) {
            editRow(row, true, true, true, true, true, true, false, false,
                    true, true);
        }

        function editRow(row, numeroOrdenDisabled, rutClienteDisabled,
                rutHuespedDisabled, nombreHuespedDisabled, apellidoHuespedDisabled, numeroHabitacionDisabled, 
                editDisabled, deleteDisabled, saveDisabled, cancelDisabled) {


            var txtNumeroOrden = "txt-numero-orden-" + row;
            var txtRutCliente = "txt-rut-cliente-" + row;
            var txtRutHuesped = "txt-rut-huesped-" + row;
            var txtNombreHuesped = "txt-nombre-huesped-" + row;
            var txtApellidoHuesped = "txt-apellido-huesped-" + row;
            var txtNumeroHabitacion = "txt-numero-habitacion-" + row;
            var btnEdit = 'bnt-edit-' + row;
            var btnDelete = 'bnt-delete-' + row;
            var btnSave = 'bnt-save-' + row;
            var btnCancel = 'bnt-cancel-' + row;

            document.getElementById(txtNumeroOrden).disabled = numeroOrdenDisabled;
            document.getElementById(txtRutCliente).disabled = rutClienteDisabled;
            document.getElementById(txtRutHuesped).disabled = rutHuespedDisabled;
            document.getElementById(txtNombreHuesped).disabled = nombreHuespedDisabled;
            document.getElementById(txtApellidoHuesped).disabled = apellidoHuespedDisabled;
            document.getElementById(txtNumeroHabitacion).disabled = numeroHabitacionDisabled;
            document.getElementById(btnEdit).hidden = editDisabled;
            document.getElementById(btnDelete).hidden = deleteDisabled;
            document.getElementById(btnSave).hidden = saveDisabled;
            document.getElementById(btnCancel).hidden = cancelDisabled;
        }

        function deleteRow(row) {
            var id = document.getElementById("txt-numero-orden-" + row).value;
            cancelRow(row);

            $.ajax({
                url: '${pageContext.request.contextPath}/DeleteOrdenCompraServlet?codigo=' + id,
                method: 'POST',
                success: window.location.reload()
            });

        }
        
        function saveRow(row) {

            var numero_orden = document.getElementById("txt-numero-orden-" + row).value;
            var rut_cliente = document.getElementById("txt-rut-cliente-" + row).value;
            var rut_huesped = document.getElementById("txt-rut-huesped-" + row).value;
            var nombre_huesped = document.getElementById("txt-nombre-huesped-" + row).value;
            var apellido_huesped = document.getElementById("txt-apellido-huesped-" + row).value;
            var numero_habitacion = document.getElementById("txt-numero-habitacion-" + row).value;
            
            cancelRow(row);
            const Http = new XMLHttpRequest();
            const url = '${pageContext.request.contextPath}/UpdateOrdenCompraServlet?numero_orden=' + numero_orden 
                    + '&rut_cliente=' + rut_cliente
                    + '&rut_huesped=' + rut_huesped + '&nombre_huesped=' + nombre_huesped
                    + '&apellido_huesped=' + apellido_huesped + '&numero_habitacion=' + numero_habitacion;
            Http.open('POST', url);
            Http.send();
        }
        
    </script>
    
</html>
