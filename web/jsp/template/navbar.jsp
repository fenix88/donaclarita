<%-- 
    Document   : navbar
    Created on : 27-05-2020, 1:39:39
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar" style="background-color: #333333">
    <a class="nav-link" href="Principal.jsp" style="color: white"><span class="navbar-toggler-icon"></span> Home </a>
    <div class="text-center" style="color: white">
        <h3><strong>Hostal Doña Clarita</strong></h3>
    </div>
    <div class="dropdown" >
        <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar Sesion</a>
        <div class="dropdown-menu text-center" >
            <a><img src="${pageContext.request.contextPath}/img/icono_usuario.jpg" height="40" width="40" ></a><br>
            <a>
                User: <c:if test="${not empty sessionScope.user}">
                    <i class="fas fa-user">${sessionScope.user}</i>
                </c:if>
            </a>
            <a>example@gmail.com</a>
            <div class="dropdown-divider" ></div>
            <a href="${pageContext.request.contextPath}/index.jsp" class="dropdown-item" > Salir </a>
        </div>
    </div>
</nav>
