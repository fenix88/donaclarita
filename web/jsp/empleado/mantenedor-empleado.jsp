<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
        <div class="container" >
            <h1>Modulo Empleados</h1>
            <br>
            <br>

            <form action="#" method="POST">						
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-rut-empleado" placeholder="" maxlength="10">
                    </div>                    
                </div>
                
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-nombre-empleado" placeholder="" >
                    </div> 
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Apellido Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-apelldo-empleado" placeholder="" >
                    </div>  
                </div>
                
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select class="form-control" id="cbo-cargo-empleado">
                            <option value=""> Selecionar Cargo </option>
                            <option>Mantenedor Productos</option>
                            <option>Mantenedor Pedidos</option>
                            <option>Facturacion</option>
                            <option>Recepcion Huespedes</option>
                            <option>Gestion Proveedor</option>
                        </select>
                        
                    </div>  
                </div>
                
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Usuario Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-usuario-empleado" placeholder="" >
                    </div> 
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Contraseña</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-password" placeholder="" >
                    </div>  
                </div>
                <br>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="button" class="btn btn-success">Agregar Empleado</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</body>
</html>
