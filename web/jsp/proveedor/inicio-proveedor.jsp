<%-- 
    Document   : Inicio_Proveedor
    Created on : 18-05-2020, 4:06:31
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
    </head>
    <body>
        
        <jsp:include page="../template/navbar.jsp"/>

         <div class="container mt-4" >
            

            <div class="card" style="width: 13rem;">
                <img  src="${pageContext.request.contextPath}/img/mantenedor-orden-compra.jpg" height="180" width="180" style="margin-left: 15px; margin-right: 15px;">
                
                <div class="card-body">
                    <h5 class="card-title">Listar Orden Pedido</h5>
                    <p class="card-text"></p>
                    <a href="${pageContext.request.contextPath}/orden-pedido-servlet" class="btn btn-primary btn-block" style="text-align: center" > Entrar </a>
                </div>
            </div>

        </div>
    </body>
</html>
