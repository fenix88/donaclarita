<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
        
        <div class="container mt-4" >
            
            <h1>Buscar Orden de Pedido</h1>
            <br>
            <br>
            <div class="row row-cols-1 row-cols-md-4">
            <form class="form-inline">
               

            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>N° Orden</th>
                        <th>Fecha Creacion</th>
                        <th>Nombre Producto</th>
                        <th>Rut Proveedor</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${lista}" var="p" >
                        <tbody> 
                            <tr>
                                <td><input type="text" value="${p.ordenPedidoIdPedido.idPedido}"/></td>
                                <td><input type="text" value="${p.ordenPedidoIdPedido.fechaCreacion}"/></td>
                                <td><input type="text" value="${p.nombreProducto}"/></td>
                                <td><input type="text" value="${p.proveedoresRutProveedor.rutProveedor}"/></td>
                            </tr>
                        </tbody>
                    </c:forEach>
                </tbody>
            </table>

               <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="button" class="btn btn-success">Ver Detalle</button>
                    </div>
                </div>

               <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="button" class="btn btn-warning">Imprimir</button>
                    </div>
                </div>

            </form>
        </div>
        </div>
    </body>
</html>
