/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Clientes;
import cl.duoc.model.Facturas;
import cl.duoc.negocio.ClientesNegocio;
import cl.duoc.negocio.FacturaNegocio;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class FacturaServlet extends HttpServlet {
    
    FacturaNegocio negocio;
    ClientesNegocio clienteNegocio;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new FacturaNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Facturas> dfacturas = negocio.listarTodo();
            LOGGER.info("listado: " + dfacturas.size());
            session.setAttribute("lista", dfacturas);
            response.sendRedirect("jsp/cliente/buscar-factura.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
