/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Huespedes;
import cl.duoc.negocio.HuespedesNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class DeleteHuespedServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    HuespedesNegocio negocio;
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("INICIANDO LISTADO HUESPEDES");
        try {
            negocio = new HuespedesNegocio();
            List<Huespedes> dhuespedes = negocio.listarTodo();
            LOGGER.info("listado huespredes: " + dhuespedes.size());
            session.setAttribute("lista", dhuespedes);
            response.sendRedirect("jsp/empleado/mantenedor-huesped.jsp"); 
            
        } catch (Exception e) {
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("DELETE HUESPED");
        Huespedes hpd = new Huespedes();
        
        try {  
            negocio = new HuespedesNegocio();
            HttpSession session = request.getSession();
            //Object regionesSession = session.getAttribute("productos");
             String rut = request.getParameter("rut");
             hpd.setRutHuesped(rut);
                          
             negocio.eliminar(hpd);
            
            LOGGER.info("Retorno negocio");

            //guardar el huesped para listarlo nuevamente
            negocio = new HuespedesNegocio();
            List<Huespedes> dhuespedes = negocio.listarTodo();
            LOGGER.info("listado huespredes: " + dhuespedes.size());
            session.setAttribute("lista", dhuespedes);
            response.sendRedirect("jsp/empleado/mantenedor-huesped.jsp"); 
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }

}
