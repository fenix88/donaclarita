/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;


import cl.duoc.model.RegistroUsuario;
import cl.duoc.negocio.RegistroUsuarioNegocio;
import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Grupo 1
 */
public class RegistrarUsuarioServlet extends HttpServlet {

     private static final Logger LOGGER = Logger.getLogger(RegistrarUsuarioServlet.class.getName());
    
    RegistroUsuarioNegocio negocio;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RegistroUsuario u = new RegistroUsuario();
        negocio = new RegistroUsuarioNegocio();
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
        //String accion=request.getParameter("btn_registrar_usuario");
        
        String rut = request.getParameter("txt_reg_rut");
        String usuario = request.getParameter("txt_reg_user");
        String password = request.getParameter("txt_reg_pass");
        String rol_usuario = request.getParameter("sl_rol");
        
         LOGGER.info("rut: " +  rut);
         LOGGER.info("usuario : " + usuario);
         LOGGER.info("password : " + password);
         LOGGER.info("rol_usuario : " + rol_usuario);
        
        u.setRutUsuario(rut);
        u.setNombreUsuario(usuario);
        u.setPassword(password);
        u.setRolUsuario(rol_usuario);
       
        boolean resultado = negocio.agregarUsuario(u);
        
        request.getRequestDispatcher("index.jsp").forward(request, response);
        
        
        } catch (Exception ex) {
            
                String loginError = ex.getMessage();
                HttpSession session = request.getSession();

                session.setAttribute("error", loginError);
                response.sendRedirect("registrar.jsp");
        }         
    }   
}
