

package cl.duoc.servlet;

import cl.duoc.model.Clientes;
import cl.duoc.model.Habitaciones;
import cl.duoc.model.Huespedes;
import cl.duoc.model.OrdenesCompra;
import cl.duoc.negocio.HuespedesNegocio;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Grupo 1
 */
public class HuespedesServlet extends HttpServlet {
    
    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    HuespedesNegocio negocio;
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("INICIANDO LISTADO HUESPEDES");
        try {
            negocio = new HuespedesNegocio();
            List<Huespedes> dhuespedes = negocio.listarTodo();
            LOGGER.info("listado huespredes: " + dhuespedes.size());
            session.setAttribute("lista", dhuespedes);
            response.sendRedirect("jsp/empleado/mantenedor-huesped.jsp"); 
            
        } catch (Exception e) {
        }
    }
      
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        Huespedes datoh = new Huespedes();
        HttpSession session = request.getSession();
        LOGGER.info("INICIANDO REGISRTRAR HUESPEDES");
        
        try {
        negocio = new HuespedesNegocio();
            
        String rut = request.getParameter("txt_rut");
        String nombre = request.getParameter("txt_nombre");
        String apellido = request.getParameter("txt_apellido");
        
        int id_habitacion  = Integer.parseInt(request.getParameter("cbo-numero-habitacion"));
        Habitaciones hbt = new Habitaciones();
        hbt.setIdHabitacion(id_habitacion);
        
        int numero_orden  = Integer.parseInt(request.getParameter("cbo-orden-pedido"));
        OrdenesCompra orc = new OrdenesCompra();
        orc.setNumeroOrden(numero_orden);
        
        String rut_clientes  = request.getParameter("cbo-rut-cliente");
        Clientes clt = new Clientes();
        clt.setRutCliente(rut_clientes);
        
   
        datoh.setRutHuesped(rut);
        datoh.setNombreHuesped(nombre);
        datoh.setApellidoHuesped(apellido);
        datoh.setHabitacionesIdHabitacion(hbt);
        datoh.setOrdenesCompraNumeroOrden(orc);
        datoh.setClientesRutCliente(clt);
        
        LOGGER.info("LLAMANDO NEGOCIO HUESPEDES");
        negocio.agregar(datoh);
        
        
       // negocio = new HuespedesNegocio();
        List<Huespedes> dhuespedes = negocio.listarTodo();
        LOGGER.info("listado huespredes: " + dhuespedes.size());
        session.setAttribute("lista", dhuespedes);
        response.sendRedirect("jsp/empleado/mantenedor-huesped.jsp"); 
 
        } catch (Exception e) {
            
            e.printStackTrace();
        }  
    
    } 
  
    
}
