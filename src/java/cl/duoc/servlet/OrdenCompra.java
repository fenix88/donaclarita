/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Huespedes;
import cl.duoc.negocio.OrdenCompraNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class OrdenCompra extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    OrdenCompraNegocio negocio;  
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        Huespedes datop = new Huespedes();
        try {  
            //String accion=request.getParameter("accion");
  
            String rut = req.getParameter("txt_rut");
            String usuario = req.getParameter("txt_usuario");
            String password = req.getParameter("txt_password");
            String nombre = req.getParameter("txt_nombre");
            String direccion = req.getParameter("txt_direccion");
            String rubro = req.getParameter("txt_rubro");
            int telefono = Integer.parseInt(req.getParameter("txt_telefono"));
            String correo = req.getParameter("txt_correo");
            
            /*
            datop.setRubroProveedor(rut);
            datop.setUsuarioProveedor(usuario);
            datop.setPassword(password);
            datop.setNombreProveedor(nombre);
            datop.setDireccionProveedor(direccion);
            datop.setRubroProveedor(rubro);
            datop.setTelefono(telefono);
            datop.setCorreoElectronico(correo);
            */
                  
            LOGGER.info("rut: " +  rut);
            LOGGER.info("usuario : " + usuario);
            LOGGER.info("password : " + password);
            LOGGER.info("nombre : " + nombre);
            LOGGER.info("direccion : " + direccion);
            LOGGER.info("rubro : " + rubro);
            LOGGER.info("telefono : " + telefono);
            LOGGER.info("correo : " + correo);
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("VALIDATE");

        try {
            response.sendRedirect("jsp/cliente/mantenedor-orden-compra.jsp"); 
            
        } catch (Exception e) {
            
        }
            
    }

}
