/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Proveedores;
import cl.duoc.negocio.ProveedorNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class DeleteProveedorServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    ProveedorNegocio negocio;
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new ProveedorNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Proveedores> dproveedores = negocio.listarTodo();
            LOGGER.info("listado: " + dproveedores.size());
            session.setAttribute("lista", dproveedores);
            response.sendRedirect("jsp/empleado/mantenedor-proveedor.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("DELETE PROVEEDORES");
        Proveedores prd = new Proveedores();
        
        try {  
            negocio = new ProveedorNegocio();
            HttpSession session = request.getSession();
            //Object regionesSession = session.getAttribute("productos");
             String codigo = request.getParameter("codigo");
             prd.setRutProveedor(codigo);
                          
             negocio.eliminar(prd);
            
            LOGGER.info("Lamada negocio");
        //regionNegocio.borrar(Integer.valueOf(id)); 
            //guardar el producto para listarlo nuevamente
            List<Proveedores> dproveedores = negocio.listarTodo();
            LOGGER.info("listado: " + dproveedores.size());
            session.setAttribute("lista", dproveedores);
            response.sendRedirect("jsp/empleado/mantenedor-proveedor.jsp"); 
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }

}
