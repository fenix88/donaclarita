/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Habitaciones;
import cl.duoc.model.Proveedores;
import cl.duoc.negocio.HabitacionesNegocio;
import cl.duoc.negocio.ProveedorNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class ListarHabitacionesServlet extends HttpServlet {

     private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
     HabitacionesNegocio negocio;
     
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new HabitacionesNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Habitaciones> dhabitaciones = negocio.listarTodo();
            LOGGER.info("listado: " + dhabitaciones.size());
            session.setAttribute("lista", dhabitaciones);
            response.sendRedirect("jsp/cliente/listar-habitaciones.jsp"); 

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
