/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Empleados;
import cl.duoc.model.OrdenPedido;
import cl.duoc.model.Platos;
import cl.duoc.model.Productos;
import cl.duoc.model.Proveedores;
import cl.duoc.negocio.ProductoNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class ProductoServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    ProductoNegocio negocio;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("VALIDATE");
        try {  
            negocio = new ProductoNegocio();
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Productos datop = new Productos();
        HttpSession session = request.getSession();
        try {  
            negocio = new ProductoNegocio();

            String codigo = request.getParameter("txt_codigo");
            String nombre = request.getParameter("txt_nombre");
            String descripcion = request.getParameter("txt_descripcion");
            int stock = Integer.parseInt(request.getParameter("txt_stock"));
            int stock_critico = Integer.parseInt(request.getParameter("txt_stock_critico"));
            int precio = Integer.parseInt(request.getParameter("txt_precio"));
            Date fecha = java.sql.Date.valueOf(request.getParameter("txt_fecha"));

            String rut_empleado  = request.getParameter("cboEmpleado");
            Empleados emp = new Empleados();
            emp.setRutEmpleado(rut_empleado);

            int orden = Integer.parseInt(request.getParameter("cboOrden"));
            OrdenPedido ord = new OrdenPedido();
            ord.setIdPedido(orden);

            String rut_proveedor = request.getParameter("cboProveedor");
            Proveedores prv = new Proveedores();
            prv.setRutProveedor(rut_proveedor);

            datop.setCodigoProducto(codigo);
            datop.setNombreProducto(nombre);
            datop.setDescripcionProducto(descripcion);
            datop.setStock(stock);
            datop.setStockCritico(stock_critico);
            datop.setPrecio(precio);
            datop.setFechaRecepcion(fecha);
            datop.setEmpleadosRutEmpleado(emp);
            datop.setOrdenPedidoIdPedido(ord);
            datop.setProveedoresRutProveedor(prv);

            LOGGER.info("rut: " +  codigo);
            LOGGER.info("nombre : " + nombre);
            LOGGER.info("descripcion : " + descripcion);
            LOGGER.info("stock_critico : " + stock_critico);
            LOGGER.info("precio : " + precio);
            LOGGER.info("fecha : " + fecha);
            LOGGER.info("empleado_rut : " + emp);
            LOGGER.info("orden_pedido : " + ord);
            LOGGER.info("proveedor_rut : " + prv);


            negocio.agregar(datop);
            negocio = new ProductoNegocio();
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 
            
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp");

        } catch (Exception e) {

            e.printStackTrace();
        }

    }
    
    
}
