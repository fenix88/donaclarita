/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Productos;
import cl.duoc.negocio.OrdenPedidoNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class OrdenPedidoServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    OrdenPedidoNegocio negocio;
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new OrdenPedidoNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Productos> dplatos = negocio.listarOrdenPedido();
            LOGGER.info("listado: " + dplatos.size());
            session.setAttribute("lista", dplatos);
            response.sendRedirect("jsp/proveedor/listar-orden-pedido.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
