/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Empleados;
import cl.duoc.model.OrdenPedido;
import cl.duoc.model.Productos;
import cl.duoc.model.Proveedores;
import cl.duoc.negocio.ProductoNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class DeleteProductoServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    ProductoNegocio negocio;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("VALIDATE");
        try {  
            negocio = new ProductoNegocio();
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("DELETE PRODUCTO");
        Productos prd = new Productos();
        
        try {  
            negocio = new ProductoNegocio();
            HttpSession session = request.getSession();
            //Object regionesSession = session.getAttribute("productos");
             String codigo = request.getParameter("codigo");
             prd.setCodigoProducto(codigo);
                          
             negocio.eliminar(prd);
            
            LOGGER.info("Lamada negocio");
        //regionNegocio.borrar(Integer.valueOf(id)); 
            //guardar el producto para listarlo nuevamente
            negocio = new ProductoNegocio();
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }
}
