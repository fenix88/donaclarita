/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Clientes;
import cl.duoc.model.Facturas;
import cl.duoc.negocio.ClientesNegocio;
import cl.duoc.negocio.FacturaNegocio;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class FacturaEmpleadoServlet extends HttpServlet {

    FacturaNegocio negocio;
    ClientesNegocio clienteNegocio;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new FacturaNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Facturas> dfacturas = negocio.listarTodo();
            LOGGER.info("listado: " + dfacturas.size());
            session.setAttribute("lista", dfacturas);
            response.sendRedirect("jsp/empleado/mantenedor-facturacion.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    } 
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Facturas datof = new Facturas();
        HttpSession session = request.getSession();
        try {  
            negocio = new FacturaNegocio();

            int codigo = Integer.parseInt(request.getParameter("txt_numero_factura"));
            int id_venta = Integer.parseInt(request.getParameter("txt_id_venta"));
            int nombre = Integer.parseInt(request.getParameter("txt_precio_factura"));
            
            
            String rut_cliente = request.getParameter("cboCliente");
            Clientes clt = new Clientes();
            clt.setRutCliente(rut_cliente);
       
            datof.setNumeroFactura(codigo);
            datof.setIdVenta(id_venta);
            datof.setPrecioFactura(nombre);
            datof.setClientesRutCliente(clt);

            negocio.agregar(datof);
            
            List<Facturas> dfacturas = negocio.listarTodo();
            LOGGER.info("listado: " + dfacturas.size());
            session.setAttribute("lista", dfacturas);
            response.sendRedirect("jsp/empleado/mantenedor-facturacion.jsp"); 

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

}
