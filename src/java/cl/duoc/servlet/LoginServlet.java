/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.RegistroUsuario;
import cl.duoc.negocio.RegistroUsuarioNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class LoginServlet extends HttpServlet {

     private static final Logger LOGGER = Logger.getLogger(LoginServlet.class.getName());

    RegistroUsuarioNegocio negocio;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
         //request.getRequestDispatcher("WEB-INF/vistas/index.jsp").forward(request, response);
    }
   

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.info("VALIDATE");

        RegistroUsuario u = new RegistroUsuario();
        RegistroUsuario registroUsuario;
        negocio = new RegistroUsuarioNegocio();

        response.setContentType("text/html;charset=UTF-8");
        try {
                String user = request.getParameter("txt_user");
                String pass = request.getParameter("txt_pass");
                String rol = request.getParameter("sl_rol");
                u.setNombreUsuario(user);
                u.setPassword(pass);
                u.setRolUsuario(rol);
                registroUsuario = negocio.autenticacion(user, pass);

                if ((user == null || "".equals(user)) || (pass == null || "".equals(pass))) {

                    LOGGER.info("LOGIN ERROR");
                    String loginError = "Debe ingresar un usuario y una contraseña";
                    HttpSession session = request.getSession();

                    session.setAttribute("error", loginError);
                    response.sendRedirect("login.jsp");

                } else {

                    if ("Cliente".equals(registroUsuario.getRolUsuario())) {

                        LOGGER.info("LOGIN SUCCESS");
                        LOGGER.info("REDIRECT TO INICIO_CLIENTE");
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        response.sendRedirect("jsp/cliente/inicio-cliente.jsp");


                    } else if (registroUsuario.getRolUsuario().equals("Empleado")) {

                        LOGGER.info("LOGIN SUCCESS");
                        LOGGER.info("REDIRECT TO INICIO_EMPLEADO");
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        response.sendRedirect("jsp/empleado/inicio-empleado.jsp");


                    }else if(registroUsuario.getRolUsuario().equals("Proveedor")){

                        LOGGER.info("LOGIN SUCCESS");
                        LOGGER.info("REDIRECT TO INICIO_PROVEEDOR");
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        response.sendRedirect("jsp/proveedor/inicio-proveedor.jsp");


                    }else {

                        LOGGER.info("LOGIN ERROR");

                        String loginError =" Usuario o Contraseña incorrectos ";
                        HttpSession session = request.getSession(false);
                        session.setAttribute("error", loginError);
                        LOGGER.info("REDIRECT LOGIN");
                        response.sendRedirect("login.jsp");
                    }

                }

        } catch (Exception ex) {

                LOGGER.info("LOGIN ERROR");
                String loginError = ex.getMessage();
                HttpSession session = request.getSession();

                session.setAttribute("error", loginError);
                response.sendRedirect("login.jsp");
        }

    }

}
