/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Proveedores;
import cl.duoc.negocio.ProveedorNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class ProveedoresServlet extends HttpServlet {
    
    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    ProveedorNegocio negocio;
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        negocio = new ProveedorNegocio();
        LOGGER.info("VALIDATE");
        try {  
            List<Proveedores> dproveedores = negocio.listarTodo();
            LOGGER.info("listado: " + dproveedores.size());
            session.setAttribute("lista", dproveedores);
            response.sendRedirect("jsp/empleado/mantenedor-proveedor.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("INICIANDO AGREGAR PROVEEDOR");
        Proveedores datop = new Proveedores();
        HttpSession session = request.getSession();
        try {  
            
            negocio = new ProveedorNegocio();
  
            String rut = request.getParameter("txt_rut_proveedor");
            String usuario = request.getParameter("txt_nombre_usuario");
            String password = request.getParameter("txt_pass");
            String nombre = request.getParameter("txt_nombre_provedor");
            String direccion = request.getParameter("txt_rubro");
            String rubro = request.getParameter("txt_direccion");
            int telefono = Integer.parseInt(request.getParameter("txt_telefono"));
            String correo = request.getParameter("txt_correo_proveedor");
            
            datop.setRutProveedor(rut);
            datop.setUsuarioProveedor(usuario);
            datop.setPassword(password);
            datop.setNombreProveedor(nombre);
            datop.setDireccionProveedor(direccion);
            datop.setRubroProveedor(rubro);
            datop.setTelefono(telefono);
            datop.setCorreoElectronico(correo);
                  
            negocio.agregar(datop);
            LOGGER.info("RETORNO NEGOCIO");
            
            List<Proveedores> dproveedores = negocio.listarTodo();
            LOGGER.info("listado: " + dproveedores.size());
            session.setAttribute("lista", dproveedores);
            response.sendRedirect("jsp/empleado/mantenedor-proveedor.jsp"); 

            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }
    
}
