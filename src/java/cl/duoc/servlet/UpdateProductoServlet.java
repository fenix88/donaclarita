/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.servlet;

import cl.duoc.model.Empleados;
import cl.duoc.model.OrdenPedido;
import cl.duoc.model.Productos;
import cl.duoc.model.Proveedores;
import cl.duoc.negocio.ProductoNegocio;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fenix
 */
public class UpdateProductoServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ProveedoresServlet.class.getName());
    ProductoNegocio negocio;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.info("VALIDATE");
        try {  
            negocio = new ProductoNegocio();
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("UPDATE PRODUCTO");
        Productos datop = new Productos();
        try {  
            negocio = new ProductoNegocio();
            HttpSession session = request.getSession();

            String codigo = request.getParameter("codigoProducto");
            String nombre = request.getParameter("nombreProducto");
            String descripcion = request.getParameter("descripcionProducto");
            int stock = Integer.parseInt(request.getParameter("stock"));
            int stock_critico = Integer.parseInt(request.getParameter("stockCritico"));
            int precio = Integer.parseInt(request.getParameter("precio"));
            Date fecha = java.sql.Date.valueOf(request.getParameter("fechaRecepcion"));

            String rut_empleado  = request.getParameter("rutEmpleado");
            Empleados emp = new Empleados();
            emp.setRutEmpleado(rut_empleado);

            int orden = Integer.parseInt(request.getParameter("idPedido"));
            OrdenPedido ord = new OrdenPedido();
            ord.setIdPedido(orden);

            String rut_proveedor = request.getParameter("rutProveedor");
            Proveedores prv = new Proveedores();
            prv.setRutProveedor(rut_proveedor);
            
            LOGGER.info("ingreso valido");

            datop.setCodigoProducto(codigo);
            datop.setNombreProducto(nombre);
            datop.setDescripcionProducto(descripcion);
            datop.setStock(stock);
            datop.setStockCritico(stock_critico);
            datop.setPrecio(precio);
            datop.setFechaRecepcion(fecha);
            datop.setEmpleadosRutEmpleado(emp);
            datop.setOrdenPedidoIdPedido(ord);
            datop.setProveedoresRutProveedor(prv);

            LOGGER.info("rut: " +  codigo);
            LOGGER.info("nombre : " + nombre);
            LOGGER.info("descripcion : " + descripcion);
            LOGGER.info("stock_critico : " + stock_critico);
            LOGGER.info("precio : " + precio);
            LOGGER.info("fecha : " + fecha);
            LOGGER.info("empleado_rut : " + emp);
            LOGGER.info("orden_pedido : " + ord);
            LOGGER.info("proveedor_rut : " + prv);

            LOGGER.info("LLAMADA NEGOCIO");
            negocio.actualizar(datop);
            
            List<Productos> dproductos = negocio.listarTodo();
            LOGGER.info("listado: " + dproductos.size());
            session.setAttribute("lista", dproductos);
            response.sendRedirect("jsp/empleado/mantenedor-productos.jsp"); 

        } catch (Exception e) {

            e.printStackTrace();
        }

    }
    
    
    

}
