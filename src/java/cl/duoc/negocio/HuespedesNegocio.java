/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.HuespedesDAO;
import cl.duoc.dao.impl.HuespedesDAOImpl;
import cl.duoc.model.Huespedes;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Grupo 1
 */
public class HuespedesNegocio {
    
    private static final Logger LOGGER = Logger.getLogger(RegistroUsuarioNegocio.class.getName());
    
    HuespedesDAO huespedesDAO;
    
    public HuespedesNegocio() throws SQLException{
    
        huespedesDAO = new HuespedesDAOImpl();
    }
    

    public List listarTodo() throws Exception{
    
        return huespedesDAO.listarTodos();
    }
    
    public void agregar(Huespedes huesped){
    
        huespedesDAO.agregar(huesped);
    }
    
    public void eliminar(Huespedes huesped) {
    
        LOGGER.info("ELIMINAR NEGOCIO : " );
        huespedesDAO.eliminar(huesped);
    }  
      
}
