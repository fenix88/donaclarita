/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.PlatosDAO;
import cl.duoc.dao.impl.PlatosDAOImpl;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author fenix
 */
public class ServicioNegocio {
    
    private static final Logger LOGGER = Logger.getLogger(ServicioNegocio.class.getName());
    PlatosDAO platosDAO;
    
    public ServicioNegocio() {
        
        platosDAO = new PlatosDAOImpl();
        
    }
    
    public List listarServicio() throws Exception{
        LOGGER.info("Servicio negocio -> listarServicio");
        return platosDAO.listarTodos();
       
    }
}
