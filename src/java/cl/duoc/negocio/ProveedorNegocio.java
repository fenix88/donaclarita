/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.ProveedoresDAO;
import cl.duoc.dao.impl.ProveedoresDAOImpl;
import cl.duoc.model.Proveedores;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fenix
 */
public class ProveedorNegocio {
    
    ProveedoresDAO proveedorDAO;
    
    public ProveedorNegocio() {

        proveedorDAO = new ProveedoresDAOImpl();
        
    }
    
    public List listarTodo() throws Exception{
    
        return proveedorDAO.listarTodos();
       
    }
    
    public void agregar(Proveedores datop) throws Exception {
        
        try {
            proveedorDAO.agregar(datop);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void eliminar(Proveedores datop) throws Exception {
        
        try  {
                proveedorDAO.eliminar(datop);

            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void modificar(Proveedores datop) throws Exception {
        
    try  {
            proveedorDAO.actualizar(datop);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
}
