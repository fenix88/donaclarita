/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.ProductoDAO;
import cl.duoc.dao.impl.ProductoDAOImpl;
import cl.duoc.model.Productos;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author fenix
 */
public class ProductoNegocio {
    
    ProductoDAO productoDAO;
    
    public ProductoNegocio() throws SQLException{

        productoDAO = new ProductoDAOImpl();
    }
    
    public List listarTodo() throws Exception{
    
        return productoDAO.listarTodos();
    }
    
    
    public void agregar(Productos producto){
    
        productoDAO.agregar(producto);
    }
    
    
    public void actualizar(Productos producto) throws Exception{
    
        productoDAO.actualizar(producto);
    } 
    
    public void eliminar(Productos producto) throws Exception {
    
        productoDAO.eliminar(producto);
    }  
    
    /*
    public void borrar(int codigo){
    
        comunaDAO.borrar(codigo);
    }   
    */
    
}


