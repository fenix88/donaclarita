/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.HabitacionesDAO;
import cl.duoc.dao.impl.HabitacionesDAOImpl;
import java.util.List;

/**
 *
 * @author fenix
 */
public class HabitacionesNegocio {
        HabitacionesDAO  habitacionDAO;
    
    public HabitacionesNegocio(){
        habitacionDAO = new HabitacionesDAOImpl();
    }
    
    public List listarTodo() throws Exception{
        
        return habitacionDAO.listarTodos();
       
    }
}
