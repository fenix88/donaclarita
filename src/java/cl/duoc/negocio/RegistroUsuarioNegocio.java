package cl.duoc.negocio;

import cl.duoc.dao.ClientesDAO;
import cl.duoc.dao.EmpleadoDAO;
import cl.duoc.dao.ProveedoresDAO;
import cl.duoc.dao.RegistroUsuarioDAO;
import cl.duoc.dao.impl.ClientesDAOImpl;
import cl.duoc.dao.impl.EmpleadoDAOImpl;
import cl.duoc.dao.impl.ProveedoresDAOImpl;
import cl.duoc.dao.impl.RegistroUsuarioDAOImpl;
import cl.duoc.model.Clientes;
import cl.duoc.model.Empleados;
import cl.duoc.model.Proveedores;
import cl.duoc.model.RegistroUsuario;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistroUsuarioNegocio {

    private static final Logger LOGGER = Logger.getLogger(RegistroUsuarioNegocio.class.getName());

    RegistroUsuarioDAO usuarioDAO;
    ClientesDAO clienteDAO;
    EmpleadoDAO empleadoDAO;
    ProveedoresDAO proveedorDAO;

    public RegistroUsuarioNegocio() {

        usuarioDAO = new RegistroUsuarioDAOImpl();
        clienteDAO = new ClientesDAOImpl();
        empleadoDAO = new EmpleadoDAOImpl();
        proveedorDAO = new ProveedoresDAOImpl();
        
    }

    public RegistroUsuario autenticacion(String nombreUsuario, String pass) throws Exception {

        RegistroUsuario autenticacion = usuarioDAO.validarCuenta(nombreUsuario, pass);

        if (autenticacion == null) {

            throw new Exception("Usuario o Contraseña invalida");
        }

        return autenticacion;
    }

    public boolean agregarUsuario(RegistroUsuario usuario) throws Exception {
        boolean resultado = false;
        //agregar validaciones objeto registro
        LOGGER.info("rol : " + usuario.getRolUsuario());
        try {
            if ("Cliente".equalsIgnoreCase(usuario.getRolUsuario())) {

                LOGGER.info("Cliente : ");

                Clientes cliente = new Clientes();
                cliente.setRutCliente(usuario.getRutUsuario());
                cliente.setUsuarioEmpresa(usuario.getNombreUsuario());
                cliente.setPassword(usuario.getPassword());
                usuario.setClientesRutCliente(cliente);
            //Llamar a clienteDAO y guardar.

                clienteDAO.agregarClientes(cliente);

            } else if ("Empleado".equalsIgnoreCase(usuario.getRolUsuario())) {

                LOGGER.info("Empleado : ");

                Empleados empleado = new Empleados();
                empleado.setUsuarioEmpleado(usuario.getNombreUsuario());
                empleado.setPassword(usuario.getPassword());
                empleado.setRutEmpleado(usuario.getRutUsuario());
                usuario.setEmpleadoRutEmpleado(empleado);
                //Llamar a empleadoDAO y guardar.
                empleadoDAO.agregarEmpleado(empleado);

            } else if ("Proveedor".equalsIgnoreCase(usuario.getRolUsuario())) {

                LOGGER.info("Proveedor : ");

                Proveedores proveedor = new Proveedores();
                proveedor.setRutProveedor(usuario.getRutUsuario());
                proveedor.setUsuarioProveedor(usuario.getNombreUsuario());
                proveedor.setPassword(usuario.getPassword());
                usuario.setProveedoresRutProveedor(proveedor);
                //Llamar a proveedorDAO y guardar.
                proveedorDAO.agregarProveedores(proveedor);
                
            }

            LOGGER.info("Agregar Usuario : ");
            resultado = usuarioDAO.agregarUsuario(usuario);

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error Negocio", e);
        }

        return resultado;

    }

}
