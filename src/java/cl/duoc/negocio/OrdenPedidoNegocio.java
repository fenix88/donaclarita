/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.ProductoDAO;
import cl.duoc.dao.impl.ProductoDAOImpl;
import java.util.List;


/**
 *
 * @author Brian
 */
public class OrdenPedidoNegocio {
    
    ProductoDAO productoDAO;
    
    public OrdenPedidoNegocio() {

        productoDAO = new ProductoDAOImpl();
        
    }
    
    public List listarOrdenPedido() throws Exception{
    
        return productoDAO.listarOrdenPedido(null);
    }
    
}
