/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.negocio;

import cl.duoc.dao.FacturaDAO;
import cl.duoc.dao.ProveedoresDAO;
import cl.duoc.dao.impl.FacturaDAOImpl;
import cl.duoc.model.Facturas;
import java.util.List;

/**
 *
 * @author fenix
 */
public class FacturaNegocio {
    
    FacturaDAO facturaDAO;
    
    public FacturaNegocio() {

        facturaDAO = new FacturaDAOImpl();
        
    }
    
    public List listarTodo() throws Exception{
    
        return facturaDAO.listarTodos();
       
    }
    
    public void eliminar(Facturas factura) throws Exception {
    
        facturaDAO.eliminar(factura);
    }  
    
    public void agregar(Facturas factura){
    
        facturaDAO.agregar(factura);
    }
    
    
}
