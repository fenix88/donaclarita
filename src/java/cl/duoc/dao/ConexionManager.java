/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConexionManager {

    private static ConexionManager INSTANCIA = null;
    private final String driverClassName = "oracle.jdbc.driver.OracleDriver";
    private final String usuario = "hostal_clarita";
    private final String clave = "admin123";
    private final String url = "jdbc:oracle:thin:@localhost:1521:orcl";
    private Connection connection;

    private ConexionManager() {
        
    }

    public static synchronized ConexionManager getInstance() {
        if (INSTANCIA == null) {
            INSTANCIA = new ConexionManager();
        }

        return INSTANCIA;
    }

    public Connection getConexion() {
        try {
            Class.forName(driverClassName);
            connection = DriverManager.getConnection(url, usuario, clave);
        
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
