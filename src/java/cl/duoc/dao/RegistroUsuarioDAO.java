/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import cl.duoc.model.RegistroUsuario;

/**
 *

* 
* Se especifica que realiza este DAO
 */
public interface RegistroUsuarioDAO extends BaseDAO<RegistroUsuario>{
    
    public RegistroUsuario validarCuenta(String nombreUsuario, String pass);
    
    public boolean agregarUsuario(RegistroUsuario usuario);
    
}


