/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import cl.duoc.model.Proveedores;

/**
 *
 * @author Brian
 */

public interface ProveedoresDAO extends BaseDAO<Proveedores>{
    
    //metodo especial para registrar proveedores en la tabla registro_usuario
    public boolean agregarProveedores(Proveedores proveedor);
    
}
