/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import cl.duoc.model.Facturas;

/**
 *
 * @author fenix
 */
public interface FacturaDAO extends BaseDAO<Facturas>{
    // public boolean agregarEmpleado(Empleados empleado);
    
    public Facturas buscarFactura(String rut);
     
}

