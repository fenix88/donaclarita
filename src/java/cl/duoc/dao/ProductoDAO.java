/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import cl.duoc.model.Facturas;
import cl.duoc.model.Productos;
import java.util.List;

/**
 *
 * @author fenix
 */
public interface ProductoDAO extends BaseDAO<Productos>{
    public List listarOrdenPedido(Productos producto);
}

