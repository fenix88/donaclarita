/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao;

import cl.duoc.model.Clientes;
import cl.duoc.model.Habitaciones;
import cl.duoc.model.Huespedes;
import cl.duoc.model.OrdenesCompra;
import java.util.List;

/**
 *
 * @author Grupo 1
 */
public interface HuespedesDAO extends BaseDAO<Huespedes> {
   
     public List listarOrdenCompra(Huespedes huesped);
}

