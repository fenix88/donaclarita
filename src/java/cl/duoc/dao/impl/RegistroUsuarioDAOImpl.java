/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.RegistroUsuarioDAO;
import cl.duoc.model.RegistroUsuario;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;
import java.util.logging.Level;

public class RegistroUsuarioDAOImpl implements RegistroUsuarioDAO {

    private Connection connection;
    ResultSet rs;

    public RegistroUsuarioDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public RegistroUsuarioDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }

    @Override
    public boolean agregar(RegistroUsuario elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(RegistroUsuario elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(RegistroUsuario elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RegistroUsuario> listarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RegistroUsuario validarCuenta(String nombreUsuario, String pass) {
        RegistroUsuario user = null;
        try {

            PreparedStatement query = connection.prepareStatement("SELECT * FROM registro_usuario WHERE nombre_usuario = ? AND password = ? ");
            query.setString(1, nombreUsuario);
            query.setString(2, pass);
            rs = query.executeQuery();
            while (rs.next()) {

                user = new RegistroUsuario();
                user.setNombreUsuario(rs.getString("nombre_usuario"));
                user.setPassword(rs.getString("password"));
                user.setRolUsuario(rs.getString("rol_usuario"));

            }
        } catch (Exception e) {
            System.out.println("Error de SQL: " + e.getMessage());
        }
        return user;
    }

    @Override
    public RegistroUsuario buscar(RegistroUsuario elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean agregarUsuario(RegistroUsuario usuario) {
        LOGGER.info("iniciando registrar cliente : " );
        boolean resultado = false;
                                                    
        try {
            
            String sql= "INSERT INTO registro_usuario (rut_usuario,nombre_usuario,password,rol_usuario,"
                    + "CLIENTES_RUT_CLIENTE,EMPLEADOS_RUT_EMPLEADO,PROVEEDORES_RUT_PROVEEDOR"
                    + ") values(?,?,?,?,?,?,?)"; 
    
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
          
            preparedStatement.setString (1, usuario.getRutUsuario());
            preparedStatement.setString (2, usuario.getNombreUsuario());
            preparedStatement.setString (3, usuario.getPassword());
            preparedStatement.setString (4, usuario.getRolUsuario());
            

            if (usuario.getClientesRutCliente() != null && usuario.getClientesRutCliente().getRutCliente() != null) {

                preparedStatement.setString(5, usuario.getClientesRutCliente().getRutCliente());
                preparedStatement.setNull(6, Types.VARCHAR);
                preparedStatement.setNull(7, Types.VARCHAR);
            } else if (usuario.getEmpleadoRutEmpleado() != null && usuario.getEmpleadoRutEmpleado().getRutEmpleado() != null) {

                preparedStatement.setNull(5, Types.VARCHAR);
                preparedStatement.setString(6, usuario.getEmpleadoRutEmpleado().getRutEmpleado());
                preparedStatement.setNull(7, Types.VARCHAR);
            } else if (usuario.getProveedoresRutProveedor() != null && usuario.getProveedoresRutProveedor().getRutProveedor() != null) {

                preparedStatement.setNull(5, Types.VARCHAR);
                preparedStatement.setNull(6, Types.VARCHAR);
                preparedStatement.setString(7, usuario.getProveedoresRutProveedor().getRutProveedor());
            } else {

                preparedStatement.setNull(5, Types.VARCHAR);
                preparedStatement.setNull(6, Types.VARCHAR);
                preparedStatement.setNull(7, Types.VARCHAR);

            }
                   
                
            resultado = preparedStatement.execute();
            
        } catch (Exception e) {

            LOGGER.log(Level.SEVERE, "Error Transacción", e);
        }
         LOGGER.info("finalizando registrar cliente : " );
         return resultado;
    }

}
