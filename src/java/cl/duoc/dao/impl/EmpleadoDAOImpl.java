/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.EmpleadoDAO;
import cl.duoc.model.Empleados;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Brian
 */
public class EmpleadoDAOImpl implements EmpleadoDAO {

    private Connection connection;
    ResultSet rs;

    @Override
    public boolean agregarEmpleado(Empleados empleado) {
        boolean resultado = false;
        
        try{
            
            String sql = "INSERT INTO empleados(RUT_EMPLEADO,USUARIO_EMPLEADO,PASSWORD) values(?,?,?)";
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setString(1, empleado.getRutEmpleado());
            preparedStatement.setString(2, empleado.getUsuarioEmpleado());
            preparedStatement.setString(3, empleado.getPassword());
            
            resultado = preparedStatement.execute(); 
            
            
            
        
        }catch(Exception e){
        
            e.printStackTrace();
            
        }

        return resultado;
    }

    @Override
    public boolean agregar(Empleados elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(Empleados elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Empleados elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Empleados buscar(Empleados elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Empleados> listarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
