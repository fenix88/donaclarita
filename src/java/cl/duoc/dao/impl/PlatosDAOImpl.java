/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.PlatosDAO;
import cl.duoc.model.Minutas;
import cl.duoc.model.Platos;
import cl.duoc.model.ServicioComedor;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fenix
 */
public class PlatosDAOImpl implements PlatosDAO{
    
    private static final Logger LOGGER = Logger.getLogger(PlatosDAOImpl.class.getName());

    private Connection connection;
    ResultSet rs;
    
    
    public PlatosDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public PlatosDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }
    
   
    @Override
    public boolean agregar(Platos elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(Platos elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Platos elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Platos buscar(Platos elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Platos> listarTodos() {
        LOGGER.info("platosDAOImpl ->  method listarTodos");

        List<Platos> lista = new ArrayList<>();
        StringBuilder consulta = new StringBuilder();
        consulta.append("SELECT P.nombre_plato AS NOMBRE_PLATO, M.ID_MINUTA AS IDMINUTA, P.precio AS PRECIO, M.dia AS DIA, M.detalle_dia AS DETALLE_DIA , ").
        append(" S.tipo_servicio AS TIPO_SERVICIO, S.id_comedor AS ID_COMEDOR").                
        append(" FROM platos P").
        append(" LEFT JOIN minutas M ON M.id_minuta = P.minutas_id_minuta").
        append(" LEFT JOIN servicio_comedor S ON S.id_comedor = P.servicio_comedor_id_comedor");
        
        LOGGER.info("EJECUCIÓN -> " + consulta.toString());

            try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery(consulta.toString());
            Platos dp;
            
            while (rs.next()) {               
                Minutas minuta  = new Minutas();
                minuta.setIdMinuta( rs.getInt("IDMINUTA"));
                minuta.setDia(rs.getString("DIA"));
                minuta.setDetalleDia( rs.getString("DETALLE_DIA"));
                 
                ServicioComedor servicio = new ServicioComedor();
                servicio.setIdComedor(rs.getInt("ID_COMEDOR"));
                servicio.setTipoServicio(rs.getString("TIPO_SERVICIO"));

                Platos plato = new Platos();
                plato.setMinutasIdminuta(minuta);
                plato.setServicioComedorIdComedor(servicio);
                plato.setNombrePlato(rs.getString("NOMBRE_PLATO"));
                plato.setPrecio(rs.getInt("PRECIO"));
                
                lista.add(plato);
            }
        } catch (Exception ex) {
             LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return lista;
    }   
    
}
