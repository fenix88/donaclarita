/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.ProductoDAO;
import cl.duoc.dao.ProveedoresDAO;
import cl.duoc.model.Empleados;
import cl.duoc.model.OrdenPedido;
import cl.duoc.model.Productos;
import cl.duoc.model.Proveedores;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fenix
 */
public class ProductoDAOImpl implements ProductoDAO{
    
    private static final Logger LOGGER = Logger.getLogger(ProductoDAOImpl.class.getName());
    
    private Connection connection;
    ResultSet rs;
    
    public ProductoDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public ProductoDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }

    @Override
    public boolean agregar(Productos producto) {
        LOGGER.info("iniciando registrar producto : " );
        boolean resultado = false;
            
        String sql = "INSERT INTO productos(codigo_producto,nombre_producto,descripcion_producto,stock,"
                + "stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,"
                + "proveedores_rut_proveedor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        
                 LOGGER.info("EJECUCIÓN -> " + sql);
        try {                         
             
         
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
                        
            preparedStatement.setString(1, producto.getCodigoProducto());
            preparedStatement.setString(2, producto.getNombreProducto());
            preparedStatement.setString(3, producto.getDescripcionProducto());
            preparedStatement.setInt(4, producto.getStock());
            preparedStatement.setInt(5, producto.getStockCritico());
            preparedStatement.setInt(6, producto.getPrecio());
            preparedStatement.setDate(7, producto.getFechaRecepcion());
            preparedStatement.setString(8, producto.getEmpleadosRutEmpleado().getRutEmpleado());
            preparedStatement.setInt(9, producto.getOrdenPedidoIdPedido().getIdPedido());
            preparedStatement.setString(10, producto.getProveedoresRutProveedor().getRutProveedor());
            
           resultado = preparedStatement.execute();
                     
        } catch (Exception ex) {
             LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return resultado;
    }

    @Override
    public boolean actualizar(Productos producto) {
        LOGGER.info("iniciando actualizar producto : " );
        String querySQL = ""
        + "UPDATE productos SET "
        + "codigo_producto = ?, "
        + "nombre_producto = ?, "
        + "descripcion_producto = ?, "
        + "stock = ?, "
        + "stock_critico = ?, "
        + "precio = ?, "
        + "fecha_recepcion = ?, "
        + "empleados_rut_empleado = ?, "
        + "orden_pedido_id_pedido = ?, "
        + "proveedores_rut_proveedor = ?, "
        + "WHERE "
        + "codigo_producto = ?";
        
        LOGGER.info("EJECUCIÓN -> " + querySQL.toString());
        
        try {

            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(querySQL);

            preparedStatement.setString (1, producto.getCodigoProducto());
            preparedStatement.setString (2, producto.getNombreProducto());
            preparedStatement.setString (3, producto.getDescripcionProducto());
            preparedStatement.setInt (4, producto.getStock());
            preparedStatement.setInt (5, producto.getStockCritico());
            preparedStatement.setInt (6, producto.getPrecio());  
            preparedStatement.setDate (7, producto.getFechaRecepcion());  
            preparedStatement.setString (8, producto.getEmpleadosRutEmpleado().getRutEmpleado());  
            preparedStatement.setInt (9, producto.getOrdenPedidoIdPedido().getIdPedido());  
            preparedStatement.setString (10, producto.getProveedoresRutProveedor().getRutProveedor());             

            preparedStatement.execute();
            return true;

        } catch (Exception e) {

            LOGGER.log(Level.SEVERE, "Error Transacción", e);
        }
        LOGGER.info("finalizando actualizar Producto : " );
        return false;
    }

    @Override
    public boolean eliminar(Productos producto) {
        LOGGER.info("iniciando eliminar producto : " );
        boolean eliminar=false;
        
        String sql=(" DELETE FROM productos WHERE codigo_producto = " + producto.getCodigoProducto());
    
        LOGGER.info("EJECUCIÓN -> " + sql.toString());
        
            try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
                       
            eliminar = preparedStatement.execute();
            
            eliminar=true;   
              
            //boolean ejecucion = ps.execute();
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }	
            return eliminar;
    }
    
    
    @Override
    public Productos buscar(Productos elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Productos> listarTodos() {
        List<Productos> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM PRODUCTOS ORDER BY "
                + "codigo_producto";
        LOGGER.info("EJECUCIÓN -> " + querySQL.toString());
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Productos dp;
            
            while (resultSet.next()) {
                dp = new Productos();
                dp.setCodigoProducto(resultSet.getString("codigo_producto"));
                dp.setNombreProducto(resultSet.getString("nombre_producto"));
                dp.setDescripcionProducto(resultSet.getString("descripcion_producto"));
                dp.setStock(resultSet.getInt("stock"));
                dp.setStockCritico(resultSet.getInt("stock_critico"));
                dp.setPrecio(resultSet.getInt("precio"));
                dp.setFechaRecepcion(resultSet.getDate("fecha_recepcion"));
                
                Empleados emp = new Empleados();
                emp.setRutEmpleado(resultSet.getString("empleados_rut_empleado"));
                dp.setEmpleadosRutEmpleado(emp);
                
                OrdenPedido opd = new OrdenPedido();
                opd.setIdPedido(resultSet.getInt("orden_pedido_id_pedido"));
                dp.setOrdenPedidoIdPedido(opd);
                
                Proveedores pvr = new Proveedores();
                pvr.setRutProveedor(resultSet.getString("proveedores_rut_proveedor"));
                dp.setProveedoresRutProveedor(pvr);
                
                lista.add(dp);
            }
            
        } catch (Exception ex) {
             LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return lista;
    }
    
    /*
    @Override
    public List<Platos> listarTodos() {
        LOGGER.info("platosDAOImpl ->  method listarTodos");

        List<Platos> lista = new ArrayList<>();
        StringBuilder consulta = new StringBuilder();
        consulta.append("SELECT P.nombre_plato AS NOMBRE_PLATO, M.ID_MINUTA AS IDMINUTA, P.precio AS PRECIO, M.dia AS DIA, M.detalle_dia AS DETALLE_DIA , ").
        append(" S.tipo_servicio AS TIPO_SERVICIO, S.id_comedor AS ID_COMEDOR").                
        append(" FROM platos P").
        append(" LEFT JOIN minutas M ON M.id_minuta = P.minutas_id_minuta").
        append(" LEFT JOIN servicio_comedor S ON S.id_comedor = P.servicio_comedor_id_comedor");
        
        LOGGER.info("EJECUCIÓN -> " + consulta.toString());

            try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery(consulta.toString());
            Platos dp;
            
            while (rs.next()) {               
                Minutas minuta  = new Minutas();
                minuta.setIdMinuta( rs.getInt("IDMINUTA"));
                minuta.setDia(rs.getString("DIA"));
                minuta.setDetalleDia( rs.getString("DETALLE_DIA"));
                 
                ServicioComedor servicio = new ServicioComedor();
                servicio.setIdComedor(rs.getInt("ID_COMEDOR"));
                servicio.setTipoServicio(rs.getString("TIPO_SERVICIO"));

                Platos plato = new Platos();
                plato.setMinutasIdminuta(minuta);
                plato.setServicioComedorIdComedor(servicio);
                plato.setNombrePlato(rs.getString("NOMBRE_PLATO"));
                plato.setPrecio(rs.getInt("PRECIO"));
                
                lista.add(plato);
            }
        } catch (Exception ex) {
             LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return lista;
    }  
    */

    @Override
    public List listarOrdenPedido(Productos producto) {
        LOGGER.info("listar orden pedido ->  ");
       List<Productos> lista = new ArrayList<>();
       
        String querySQL =  "Select pr.nombre_producto as nombre_producto,"
                        + "o.id_pedido AS numero_orden,"
                        + " o.fecha_creacion AS fecha_creacion,"
                        + " p.rut_proveedor AS rut_proveedor,"
                        + " p.nombre_proveedor AS nombre_proveedor"
                        + " FROM productos pr "
                        + " LEFT JOIN orden_pedido o ON o.id_pedido = pr.orden_pedido_id_pedido"
                        + " LEFT JOIN proveedores p ON p.rut_proveedor = pr.proveedores_rut_proveedor";
        
         LOGGER.info("EJECUCIÓN JOIN-> " + querySQL.toString());
         

            try {
                
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery(querySQL);
            
            while (rs.next()) {               
            OrdenPedido orden  = new OrdenPedido();
            orden.setIdPedido(rs.getInt("numero_orden"));
            orden.setFechaCreacion(rs.getDate("fecha_creacion"));

            Proveedores prv = new Proveedores();
            prv.setRutProveedor(rs.getString("rut_proveedor"));
            prv.setNombreProveedor(rs.getString("nombre_proveedor"));

            
            Productos prd = new Productos();
            prd.setOrdenPedidoIdPedido(orden);
            prd.setProveedoresRutProveedor(prv);
            prd.setNombreProducto(rs.getString("nombre_producto"));

            lista.add(prd);
            
            }
            
            } catch (Exception ex) {
             LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
               
            return lista;
       
        }      
    
}

