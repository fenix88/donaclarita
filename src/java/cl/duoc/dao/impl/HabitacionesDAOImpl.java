/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.HabitacionesDAO;
import cl.duoc.model.Habitaciones;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Grupo 1
 * 
 */
public class HabitacionesDAOImpl implements HabitacionesDAO {

    private Connection connection;
    ResultSet rs;
    
    public HabitacionesDAOImpl(Connection connection) {
        this.connection = connection;
    }
    
    public HabitacionesDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }
    
    @Override
    public boolean agregar(Habitaciones elemento) {
        String querySQL = ""
        + "UPDATE HABITACIONES SET "
        + "id_habitacion = ?, "
        + "numero_habitacion = ?, "
        + "tipo_cama = ?, "
        + "accesorio = ?, "
        + "precio = , "
        + "cantidad_accesorios = ?, "
        + "WHERE "
        + "id_habitacion = ?";
        
        try {

            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(querySQL);

            preparedStatement.setInt (1, elemento.getIdHabitacion());
            preparedStatement.setInt (2, elemento.getNumeroHabitacion());
            preparedStatement.setString (3, elemento.getTipoCama());
            preparedStatement.setString (4, elemento.getAccesorio());
            preparedStatement.setInt (4, elemento.getPrecio());
            preparedStatement.setInt (4, elemento.getCantidadAccesorios());  

            preparedStatement.execute();
            return true;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return false;
    }
    
    @Override
    public boolean actualizar(Habitaciones elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Habitaciones elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Habitaciones buscar(Habitaciones elemento) {
        Habitaciones userData = new Habitaciones();
        try{
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement find = connection.prepareStatement("SELECT * FROM habitaciones where id_habitacion = ?");
            find.setInt(1, elemento.getIdHabitacion());
            ResultSet rs = find.executeQuery();
            while(rs.next()){
                userData.setIdHabitacion(rs.getInt("id_habitacion"));
                userData.setNumeroHabitacion(rs.getInt("numero_habitacion"));
                userData.setTipoCama(rs.getString("tipo_cama"));
                userData.setAccesorio(rs.getString("accesorio"));
                userData.setPrecio(rs.getInt("precio"));
                userData.setCantidadAccesorios(rs.getInt("cantidad_accesorios"));        
            }
            find.close();
        } catch (Exception e){
            System.out.println("Error de SQL (Buscar Habitacion): "+e.getMessage());
        }
        return userData;
    }
    
    @Override
    public List<Habitaciones> listarTodos() {
      List<Habitaciones> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM HABITACIONES" ;
        
        
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Habitaciones dh;
            
            while (resultSet.next()) {
                dh = new Habitaciones();
                dh.setNumeroHabitacion(resultSet.getInt("numero_habitacion"));
                dh.setTipoCama(resultSet.getString("tipo_cama"));
                dh.setAccesorio(resultSet.getString("accesorio"));
                dh.setPrecio(resultSet.getInt("precio"));
                dh.setCantidadAccesorios(resultSet.getInt("cantidad_accesorios"));
                
                lista.add(dh);
            }
        } catch (Exception e) {
             e.printStackTrace();
        }
        return lista;
    }
    

    
}
