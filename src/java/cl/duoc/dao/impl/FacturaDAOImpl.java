/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.FacturaDAO;
import cl.duoc.model.Clientes;
import cl.duoc.model.Facturas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fenix
 */
public class FacturaDAOImpl implements FacturaDAO {

    private static final Logger LOGGER = Logger.getLogger(PlatosDAOImpl.class.getName());

    private Connection connection;
    ResultSet rs;

    public FacturaDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public FacturaDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }

    @Override
    public boolean agregar(Facturas factura) {
        LOGGER.info("iniciando registrar factura : " );
        boolean resultado = false;
        String sql = "INSERT INTO facturas(numero_factura,id_venta,precio_factura,"
              + "clientes_rut_cliente) VALUES (?, ?, ?, ?)";
        
            LOGGER.info("EJECUCIÓN QUERY-> " + sql);
            
        try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setInt(1, factura.getNumeroFactura());
            preparedStatement.setInt(2, factura.getIdVenta());
            preparedStatement.setInt(3, factura.getPrecioFactura());
            
            preparedStatement.setString(4, factura.getClientesRutCliente().getRutCliente());
            resultado = preparedStatement.execute();
            
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
            
            return resultado;
    }
    
    
    
    @Override
    public boolean actualizar(Facturas elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Facturas factura) {
         LOGGER.info("iniciando eliminar factura : " );
          boolean eliminar=false;
        String sql=(" DELETE FROM facturas WHERE numero_factura = " + factura.getNumeroFactura());
        
        LOGGER.info("EJECUCIÓN -> " + sql.toString());
        
        try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
             eliminar = preparedStatement.execute();
             
              eliminar=true;   
            
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return eliminar;
    }
    
    /*
    
        public List<ComunaEntity> buscarTodo() {

        List<ComunaEntity> comunas = new ArrayList<>();

        StringBuilder consulta = new StringBuilder();
        consulta.append("SELECT C.CODIGO, C.NOMBRE, C.DESCRIPCION, R.CODIGO_REGION ").
                append(" R.NOMBRE, R.DESCRIPCION").                
                append(" FROM COMUNA C").
                append(" JOIN REGION R ON R.CODIGO = C.CODIGO_REGION");
    
    
    @Override
    public boolean eliminar(Productos producto) {
        LOGGER.info("iniciando eliminar producto : " );
        boolean eliminar=false;
        
        String sql=(" DELETE FROM productos WHERE codigo_producto = " + producto.getCodigoProducto());
    
        LOGGER.info("EJECUCIÓN -> " + sql.toString());
        
            try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
                       
            eliminar = preparedStatement.execute();
            
            eliminar=true;   
              
            //boolean ejecucion = ps.execute();
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }	
            return eliminar;
    }
    */

    @Override
    public Facturas buscar(Facturas elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Facturas> listarTodos() {
        LOGGER.info("facturaDAOImpl ->  method listarTodos");
        List<Facturas> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM FACTURAS";
        
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Facturas df;
            
            while (resultSet.next()) {
                df = new Facturas();
                df.setNumeroFactura(resultSet.getInt("numero_factura"));
                df.setPrecioFactura(resultSet.getInt("precio_factura"));
                df.setIdVenta(resultSet.getInt("id_venta"));
                
                Clientes cl = new Clientes();
                cl.setRutCliente(resultSet.getString("clientes_rut_cliente"));
                df.setClientesRutCliente(cl);
                
                lista.add(df);
                
            }
        } catch (Exception e) {
             e.printStackTrace();
        }
        return lista;
    }    
     
    @Override
    public Facturas buscarFactura(String rut) {
        Facturas facturaData = new Facturas();
        try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement find = connection.prepareStatement("SELECT * FROM facturas "
                    + "where clientes_rut_cliente = ?");
        } catch (Exception e) {
            System.out.println("Erro de SQL (Buscar Factura Por RUT): " + e.getMessage());
        }

        return facturaData;
    }

}
