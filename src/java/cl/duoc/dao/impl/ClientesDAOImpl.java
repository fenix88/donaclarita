/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ClientesDAO;
import cl.duoc.dao.ConexionManager;
import cl.duoc.model.Clientes;
import cl.duoc.negocio.RegistroUsuarioNegocio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brian
 */
public class ClientesDAOImpl implements ClientesDAO{
    
    private static final Logger LOGGER = Logger.getLogger(RegistroUsuarioNegocio.class.getName());
    
    private Connection connection;
    ResultSet rs;

    @Override
    public boolean agregarClientes(Clientes cliente) {
        LOGGER.info("iniciando agregar cliente : " );
        boolean resultado = false;
        
        try{
            
            String sql = "INSERT INTO clientes(rut_cliente,usuario_empresa,password) values(?,?,?)";
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, cliente.getRutCliente());
            preparedStatement.setString(2, cliente.getUsuarioEmpresa());
            preparedStatement.setString(3, cliente.getPassword());
            
            resultado = preparedStatement.execute(); 
        
        
        }catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error Transacción", e);
           
        }
        LOGGER.info("finalizando agregar cliente : " );
        return resultado;
    }

    @Override
    public boolean agregar(Clientes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(Clientes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Clientes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Clientes buscar(Clientes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Clientes> listarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
