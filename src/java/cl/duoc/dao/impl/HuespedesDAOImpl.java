    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.HuespedesDAO;
import cl.duoc.model.Clientes;
import cl.duoc.model.Habitaciones;
import cl.duoc.model.Huespedes;
import cl.duoc.model.OrdenesCompra;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupo 1
 */
public class HuespedesDAOImpl implements HuespedesDAO {
    
    private static final Logger LOGGER = Logger.getLogger(ProductoDAOImpl.class.getName());
       
    private Connection connection;
    ResultSet rs;
    
    public HuespedesDAOImpl(Connection connection) {
        this.connection = connection;
    }
    
    public HuespedesDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }

    
    @Override
    public boolean agregar(Huespedes huesped) {
        LOGGER.info("iniciando registrar huesped : " );
        boolean resultado = false;
        String sql = "INSERT INTO huespedes(rut_huesped,nombre_huesped,apellido_huesped,"
        + "habitaciones_id_habitacion,ordenes_compra_numero_orden,"
        + "clientes_rut_cliente) VALUES (?, ?, ?, ?, ?, ?)";
        
        LOGGER.info("EJECUCIÓN INSERCION HUESPEDES-> " + sql);
        
        try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setString(1, huesped.getRutHuesped());
            preparedStatement.setString(2, huesped.getNombreHuesped());
            preparedStatement.setString(3, huesped.getApellidoHuesped());
            
            preparedStatement.setInt(4, huesped.getHabitacionesIdHabitacion().getIdHabitacion());
            preparedStatement.setInt(5, huesped.getOrdenesCompraNumeroOrden().getNumeroOrden());
            preparedStatement.setString(6, huesped.getClientesRutCliente().getRutCliente());
            
            resultado = preparedStatement.execute();
            
        } catch (Exception ex) {
            
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
         return resultado; 
    }
    

    @Override
    public boolean actualizar(Huespedes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Huespedes huesped) {
       LOGGER.info("iniciando eliminar huesped : " );
       boolean eliminar=false;
       
       String sql=(" DELETE FROM huespedes WHERE rut_huesped = '" + huesped.getRutHuesped() + "'");
       
       LOGGER.info("EJECUCIÓN QUERY ELIMINAR HUESPEDES -> " + sql.toString());
       
        try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
            eliminar = preparedStatement.execute(sql);
            
            eliminar=true;   
            
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return eliminar;
       
    }

    @Override
    public Huespedes buscar(Huespedes elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Huespedes> listarTodos() {
        List<Huespedes> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM huespedes ORDER BY "
        + "rut_huesped";
        LOGGER.info("EJECUCIÓN  listarTodos Huespedes-> " + querySQL.toString());
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Huespedes dh;
            
            while (resultSet.next()) {
                dh = new Huespedes();
                dh.setRutHuesped(resultSet.getString("rut_huesped"));
                dh.setNombreHuesped(resultSet.getString("nombre_huesped"));
                dh.setApellidoHuesped(resultSet.getString("apellido_huesped"));
                
                Habitaciones hbt = new Habitaciones();
                hbt.setIdHabitacion(resultSet.getInt("habitaciones_id_habitacion"));
                dh.setHabitacionesIdHabitacion(hbt);
                
                OrdenesCompra orc = new OrdenesCompra();
                orc.setNumeroOrden(resultSet.getInt("ordenes_compra_numero_orden"));
                dh.setOrdenesCompraNumeroOrden(orc);
                
                Clientes clt = new Clientes();
                clt.setRutCliente(resultSet.getString("clientes_rut_cliente"));
                dh.setClientesRutCliente(clt);
                
                lista.add(dh);
            }
            
        } catch (Exception e) {
        }
        
        return lista;
        
    }
    
    @Override
    public List listarOrdenCompra(Huespedes huesped) {
        List<Huespedes> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM HUESPEDES";
               try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Huespedes dh;
            
            while (resultSet.next()) {
                dh = new Huespedes();
                dh.setNombreHuesped(resultSet.getString("nombre_huesped"));
                dh.setApellidoHuesped(resultSet.getString("apellido_huesped"));             
                lista.add(dh);
            }
        } catch (Exception e) {
             e.printStackTrace();
        }
        return lista;
    }
   
}
