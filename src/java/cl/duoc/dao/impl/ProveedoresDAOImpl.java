/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dao.impl;

import cl.duoc.dao.ConexionManager;
import cl.duoc.dao.ProveedoresDAO;
import cl.duoc.model.Proveedores;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brian
 */
public class ProveedoresDAOImpl implements ProveedoresDAO{
    
    private static final Logger LOGGER = Logger.getLogger(ProductoDAOImpl.class.getName());
    
    private Connection connection;
    ResultSet rs;
    
    public ProveedoresDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public ProveedoresDAOImpl() {
        connection = ConexionManager.getInstance().getConexion();
    }

    @Override
    public boolean agregarProveedores(Proveedores proveedor) {
        boolean resultado = false;
                
        try{
            
            String sql = "INSERT INTO proveedores(RUT_PROVEEDOR,USUARIO_PROVEEDOR,password) values(?,?,?)";
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, proveedor.getRutProveedor());
            preparedStatement.setString(2, proveedor.getUsuarioProveedor());
            preparedStatement.setString(3, proveedor.getPassword());
            
            resultado = preparedStatement.execute();
        
        }catch(Exception e){
        
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public boolean agregar(Proveedores proveedor) {
    LOGGER.info("iniciando registrar proveedor : " );
    boolean resultado = false;


    String sql = "INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,"
            +"nombre_proveedor,direccion_proveedor,rubro_proveedor,"
            + "telefono,correo_electronico) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
    /*
    String sql = "INSERT INTO productos(codigo_producto,nombre_producto,descripcion_producto,stock,"
        + "stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,"
        + "proveedores_rut_proveedor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    */
    

             LOGGER.info("EJECUCIÓN -> " + sql);

        try {			
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, proveedor.getRutProveedor());
            preparedStatement.setString(2, proveedor.getUsuarioProveedor());
            preparedStatement.setString(3, proveedor.getPassword());
            preparedStatement.setString(4, proveedor.getNombreProveedor());
            preparedStatement.setString(5, proveedor.getDireccionProveedor());
            preparedStatement.setString(6, proveedor.getRubroProveedor());
            preparedStatement.setInt(7, proveedor.getTelefono());
            preparedStatement.setString(8, proveedor.getCorreoElectronico());

            resultado = preparedStatement.execute();

        } catch (Exception e) {
                System.out.println("Error: método registrar");
                e.printStackTrace();
        }
        return resultado;
    }
    
    @Override
    public boolean actualizar(Proveedores proveedor) {
        
        Statement stm= null;
        boolean actualizar=false;
        
        String sql="UPDATE proveedores SET rut_proveedor='"+proveedor.getRubroProveedor()+"', "
                + "usuario_proveedor='"+proveedor.getUsuarioProveedor()+"',"
                + " password='"+proveedor.getPassword()+"',"
                + " nombre_proveedor='"+proveedor.getNombreProveedor()+"',"
                + " direccion_proveedor='"+proveedor.getDireccionProveedor()+"',"
                + " rubro_proveedor='"+proveedor.getRubroProveedor()+"',"
                + " telefono='"+proveedor.getTelefono()+"',"
                + " correo_electronico='"+proveedor.getCorreoElectronico()+"',"
                + "" +" WHERE rut_proveedor="+proveedor.getRutProveedor();
        
        try {
                stm=connection.createStatement();
                stm.execute(sql);
                actualizar=true;
        } catch (Exception e) {
                System.out.println("Error: método actualizar");
                e.printStackTrace();
        }
        return actualizar;
        
    }
    
    
    @Override
    public boolean eliminar(Proveedores proveedor) {
        LOGGER.info("iniciando eliminar proveedor : " );
        boolean eliminar=false;
        
        String sql= ("DELETE FROM proveedores WHERE rut_proveedor = '" + proveedor.getRutProveedor() + "'");
        //String sql=(" DELETE FROM huespedes WHERE rut_huesped = '" + huesped.getRutHuesped() + "'");
        LOGGER.info("EJECUCIÓN QUERY-> " + sql.toString());
            try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
            
            eliminar = preparedStatement.execute();
            eliminar=true;   
            
            } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }	
            return eliminar;
    }
    
    /*
    @Override
    public boolean eliminar(Productos producto) {
        LOGGER.info("iniciando eliminar producto : " );
        boolean eliminar=false;
        
        String sql=(" DELETE FROM productos WHERE codigo_producto = " + producto.getCodigoProducto());
    
        LOGGER.info("EJECUCIÓN -> " + sql.toString());
        
            try {
            Connection connection = ConexionManager.getInstance().getConexion();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(sql);
                       
            eliminar = preparedStatement.execute();
            
            eliminar=true;   
              
            //boolean ejecucion = ps.execute();
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }	
            return eliminar;
    }
    */
    
    @Override
    public Proveedores buscar(Proveedores elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Proveedores> listarTodos() {
        List<Proveedores> lista = new ArrayList<>();
        String querySQL = "SELECT * FROM PROVEEDORES";
        
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Proveedores dp;
            
            while (resultSet.next()) {
                dp = new Proveedores();
                dp.setRutProveedor(resultSet.getString("rut_proveedor"));
                dp.setUsuarioProveedor(resultSet.getString("usuario_proveedor"));
                dp.setPassword(resultSet.getString("password"));
                dp.setNombreProveedor(resultSet.getString("nombre_proveedor"));
                dp.setDireccionProveedor(resultSet.getString("direccion_proveedor"));
                dp.setRubroProveedor(resultSet.getString("rubro_proveedor"));
                dp.setTelefono(resultSet.getInt("telefono"));
                dp.setCorreoElectronico(resultSet.getString("correo_electronico"));
                
                lista.add(dp);
            }
        } catch (Exception e) {
             e.printStackTrace();
        }
        return lista;
    }
    
    
}
