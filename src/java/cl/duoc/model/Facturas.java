/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Facturas {

    private int numeroFactura;
    private int idVenta;
    private int precioFactura;
    private Clientes clientesRutCliente;
    
    public Facturas() {
    }

    public Facturas(int numeroFactura, int idVenta, int precioFactura, Clientes clientesRutCliente) {
        this.numeroFactura = numeroFactura;
        this.idVenta = idVenta;
        this.precioFactura = precioFactura;
        this.clientesRutCliente = clientesRutCliente;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public int getPrecioFactura() {
        return precioFactura;
    }

    public void setPrecioFactura(int precioFactura) {
        this.precioFactura = precioFactura;
    }

    public Clientes getClientesRutCliente() {
        return clientesRutCliente;
    }

    public void setClientesRutCliente(Clientes clientesRutCliente) {
        this.clientesRutCliente = clientesRutCliente;
    }

    @Override
    public String toString() {
        return "Facturas{" + "numeroFactura=" + numeroFactura + ", idVenta=" + idVenta + ", precioFactura=" + precioFactura + ", clientesRutCliente=" + clientesRutCliente + '}';
    }
    

    
}
