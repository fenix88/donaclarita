/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

import java.sql.Date;

/**
 *
 * @author Grupo 1
 */
public class Productos {

    private String codigoProducto;
    private String nombreProducto;
    private String descripcionProducto;
    private int stock;
    private int stockCritico;
    private int precio;
    private Date fechaRecepcion;
    private Empleados empleadosRutEmpleado;
    private OrdenPedido ordenPedidoIdPedido;    
    private Proveedores proveedoresRutProveedor;
    
    public Productos() {
    }

    public Productos(String codigoProducto, String nombreProducto, String descripcionProducto, int stock, int stockCritico, int precio, Date fechaRecepcion, Empleados empleadosRutEmpleado, OrdenPedido ordenPedidoIdPedido, Proveedores proveedoresRutProveedor) {
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.descripcionProducto = descripcionProducto;
        this.stock = stock;
        this.stockCritico = stockCritico;
        this.precio = precio;
        this.fechaRecepcion = fechaRecepcion;
        this.empleadosRutEmpleado = empleadosRutEmpleado;
        this.ordenPedidoIdPedido = ordenPedidoIdPedido;
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockCritico() {
        return stockCritico;
    }

    public void setStockCritico(int stockCritico) {
        this.stockCritico = stockCritico;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Empleados getEmpleadosRutEmpleado() {
        return empleadosRutEmpleado;
    }

    public void setEmpleadosRutEmpleado(Empleados empleadosRutEmpleado) {
        this.empleadosRutEmpleado = empleadosRutEmpleado;
    }

    public OrdenPedido getOrdenPedidoIdPedido() {
        return ordenPedidoIdPedido;
    }

    public void setOrdenPedidoIdPedido(OrdenPedido ordenPedidoIdPedido) {
        this.ordenPedidoIdPedido = ordenPedidoIdPedido;
    }

    public Proveedores getProveedoresRutProveedor() {
        return proveedoresRutProveedor;
    }

    public void setProveedoresRutProveedor(Proveedores proveedoresRutProveedor) {
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    @Override
    public String toString() {
        return "Productos{" + "codigoProducto=" + codigoProducto + ", nombreProducto=" + nombreProducto + ", descripcionProducto=" + descripcionProducto + ", stock=" + stock + ", stockCritico=" + stockCritico + ", precio=" + precio + ", fechaRecepcion=" + fechaRecepcion + ", empleadosRutEmpleado=" + empleadosRutEmpleado + ", ordenPedidoIdPedido=" + ordenPedidoIdPedido + ", proveedoresRutProveedor=" + proveedoresRutProveedor + '}';
    }
        
}
