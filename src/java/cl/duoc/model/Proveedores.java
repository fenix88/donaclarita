/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Proveedores {

    private String rutProveedor;
    private String usuarioProveedor;
    private String password;
    private String nombreProveedor;
    private String direccionProveedor;
    private String rubroProveedor;
    private int telefono;
    private String correoElectronico;
    
    public Proveedores() {
    }

    public Proveedores(String rutProveedor,String usuarioProveedor,String password , String nombreProveedor, String direccionProveedor, String rubroProveedor, int telefono, String correoElectronico) {
        this.rutProveedor = rutProveedor;
        this.usuarioProveedor = usuarioProveedor;
        this.password = password;
        this.nombreProveedor = nombreProveedor;
        this.direccionProveedor = direccionProveedor;
        this.rubroProveedor = rubroProveedor;
        this.telefono = telefono;
        this.correoElectronico = correoElectronico;
    }

    public String getRutProveedor() {
        return rutProveedor;
    }

    public void setRutProveedor(String rutProveedor) {
        this.rutProveedor = rutProveedor;
    }

    public String getUsuarioProveedor(){
        return usuarioProveedor;
    }
    
    public void setUsuarioProveedor(String usuarioProveedor){
        this.usuarioProveedor = usuarioProveedor;
    }
    
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getRubroProveedor() {
        return rubroProveedor;
    }

    public void setRubroProveedor(String rubroProveedor) {
        this.rubroProveedor = rubroProveedor;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Override
    public String toString() {
        return "Proveedores{" + "rutProveedor=" + rutProveedor + "usuarioProveedor" + usuarioProveedor + "password" + password 
                + ", nombreProveedor=" + nombreProveedor + ", direccionProveedor=" + direccionProveedor 
                + ", rubroProveedor=" + rubroProveedor + ", telefono=" + telefono + ", correoElectronico=" 
                + correoElectronico + '}';
    }

}
