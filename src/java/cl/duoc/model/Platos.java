/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Platos {
    
    private int idPlato;
    private String nombrePlato;
    private int precio;
    private ServicioComedor servicioComedorIdComedor;
    private Minutas minutasIdminuta;
    
    
    public Platos() {
    }

    public Platos(int idPlato, String nombrePlato, int precio, ServicioComedor servicioComedorIdComedor, Minutas minutasIdminuta) {
        this.idPlato = idPlato;
        this.nombrePlato = nombrePlato;
        this.precio = precio;
        this.servicioComedorIdComedor = servicioComedorIdComedor;
        this.minutasIdminuta = minutasIdminuta;
    }

    public int getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(int idPlato) {
        this.idPlato = idPlato;
    }

    public String getNombrePlato() {
        return nombrePlato;
    }

    public void setNombrePlato(String nombrePlato) {
        this.nombrePlato = nombrePlato;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public ServicioComedor getServicioComedorIdComedor() {
        return servicioComedorIdComedor;
    }

    public void setServicioComedorIdComedor(ServicioComedor servicioComedorIdComedor) {
        this.servicioComedorIdComedor = servicioComedorIdComedor;
    }

    public Minutas getMinutasIdminuta() {
        return minutasIdminuta;
    }

    public void setMinutasIdminuta(Minutas minutasIdminuta) {
        this.minutasIdminuta = minutasIdminuta;
    }

    @Override
    public String toString() {
        return "Platos{" + "idPlato=" + idPlato + ", nombrePlato=" + nombrePlato + ", precio=" + precio + ", servicioComedorIdComedor=" + servicioComedorIdComedor + ", minutasIdminuta=" + minutasIdminuta + '}';
    }
       
}
