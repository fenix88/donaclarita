/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1 
 */
public class Habitaciones {

    private int idHabitacion;
    private int numeroHabitacion;
    private String tipoCama;
    private String accesorio;
    private int precio;
    private int cantidadAccesorios; 
    
    public Habitaciones() {
        
    }
    
    
    public Habitaciones(int idHabitacion, int numeroHabitacion, String tipoCama, String accesorio, int precio, int cantidadAccesorios) {
        this.idHabitacion = idHabitacion;
        this.numeroHabitacion = numeroHabitacion;
        this.tipoCama = tipoCama;
        this.accesorio = accesorio;
        this.precio = precio;
        this.cantidadAccesorios = cantidadAccesorios;
    }
    
    
    public int getIdHabitacion() {
        return idHabitacion;
    }

    public void setIdHabitacion(int idHabitacion) {
        this.idHabitacion = idHabitacion;
    }

    public int getNumeroHabitacion() {
        return numeroHabitacion;
    }

    public void setNumeroHabitacion(int numeroHabitacion) {
        this.numeroHabitacion = numeroHabitacion;
    }

    public String getTipoCama() {
        return tipoCama;
    }

    public void setTipoCama(String tipoCama) {
        this.tipoCama = tipoCama;
    }

    public String getAccesorio() {
        return accesorio;
    }

    public void setAccesorio(String accesorio) {
        this.accesorio = accesorio;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getCantidadAccesorios() {
        return cantidadAccesorios;
    }

    public void setCantidadAccesorios(int cantidadAccesorios) {
        this.cantidadAccesorios = cantidadAccesorios;
    }

    @Override
    public String toString() {
        return "Habitaciones{" + "idHabitacion=" + idHabitacion + ", numeroHabitacion=" + numeroHabitacion + ", tipoCama=" + tipoCama + ", accesorio=" + accesorio + ", precio=" + precio + ", cantidadAccesorios=" + cantidadAccesorios + '}';
    }
       
}
