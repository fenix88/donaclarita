/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class ServicioComedor {

    private int idComedor;
    private String tipoServicio;
    private Facturas facturasNumeroFactura;
    
    public ServicioComedor() {
    }

    public ServicioComedor(int idComedor, String tipoServicio, Facturas facturasNumeroFactura) {
        this.idComedor = idComedor;
        this.tipoServicio = tipoServicio;
        this.facturasNumeroFactura = facturasNumeroFactura;
    }

    public int getIdComedor() {
        return idComedor;
    }

    public void setIdComedor(int idComedor) {
        this.idComedor = idComedor;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Facturas getFacturasNumeroFactura() {
        return facturasNumeroFactura;
    }

    public void setFacturasNumeroFactura(Facturas facturasNumeroFactura) {
        this.facturasNumeroFactura = facturasNumeroFactura;
    }

    @Override
    public String toString() {
        return "ServicioComedor{" + "idComedor=" + idComedor + ", tipoServicio=" + tipoServicio + ", facturasNumeroFactura=" + facturasNumeroFactura + '}';
    }
       
}
