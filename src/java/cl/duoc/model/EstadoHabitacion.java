/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class EstadoHabitacion {

    private int idHabitacionHabitacion;
    private String estadoHabitacion;
    private Habitaciones habitacionesIdHabitacion;
    
    public EstadoHabitacion() {
    }

    public EstadoHabitacion(int idHabitacionHabitacion, String estadoHabitacion, Habitaciones habitacionesIdHabitacion) {
        this.idHabitacionHabitacion = idHabitacionHabitacion;
        this.estadoHabitacion = estadoHabitacion;
        this.habitacionesIdHabitacion = habitacionesIdHabitacion;
    }

    public int getIdHabitacionHabitacion() {
        return idHabitacionHabitacion;
    }

    public void setIdHabitacionHabitacion(int idHabitacionHabitacion) {
        this.idHabitacionHabitacion = idHabitacionHabitacion;
    }

    public String getEstadoHabitacion() {
        return estadoHabitacion;
    }

    public void setEstadoHabitacion(String estadoHabitacion) {
        this.estadoHabitacion = estadoHabitacion;
    }

    public Habitaciones getHabitacionesIdHabitacion() {
        return habitacionesIdHabitacion;
    }

    public void setHabitacionesIdHabitacion(Habitaciones habitacionesIdHabitacion) {
        this.habitacionesIdHabitacion = habitacionesIdHabitacion;
    }
      
}
