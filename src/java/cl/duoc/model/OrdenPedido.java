/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

import java.sql.Date;

/**
 *
 * @author Grupo 1
 */
public class OrdenPedido {

    private int idPedido;
    private Date fechaCreacion;
    private Proveedores proveedoresRutProveedor; 
    
    public OrdenPedido() {
    }

    public OrdenPedido(int idPedido, Date fechaCreacion, Proveedores proveedoresRutProveedor) {
        this.idPedido = idPedido;
        this.fechaCreacion = fechaCreacion;
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Proveedores getProveedoresRutProveedor() {
        return proveedoresRutProveedor;
    }

    public void setProveedoresRutProveedor(Proveedores proveedoresRutProveedor) {
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    @Override
    public String toString() {
        return "ordenPedido{" + "idPedido=" + idPedido + ", fechaCreacion=" + fechaCreacion + ", proveedoresRutProveedor=" + proveedoresRutProveedor + '}';
    }
    

        
}
