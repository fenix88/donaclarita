
package cl.duoc.model;

/**
 *
 *  Grupo 1 
 */
public class RegistroUsuario {

    private String rutUsuario;
    private String nombreUsuario;
    private String password;
    private String rolUsuario;
    private Clientes clientesRutCliente;
    private Empleados empleadosRutEmpleado;
    private Proveedores proveedoresRutProveedor;
    
    public RegistroUsuario() {
    }

    public RegistroUsuario(String rutUsuario, String nombreUsuario, String password, String rolUsuario, Clientes clientesRutCliente, Empleados empleadoRutEmpleado, Proveedores proveedoresRutProveedor) {
        this.rutUsuario = rutUsuario;
        this.nombreUsuario = nombreUsuario;
        this.password = password;
        this.rolUsuario = rolUsuario;
        this.clientesRutCliente = clientesRutCliente;
        this.empleadosRutEmpleado = empleadoRutEmpleado;
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    public String getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(String rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(String rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public Clientes getClientesRutCliente() {
        return clientesRutCliente;
    }

    public void setClientesRutCliente(Clientes clientesRutCliente) {
        this.clientesRutCliente = clientesRutCliente;
    }

    public Empleados getEmpleadoRutEmpleado() {
        return empleadosRutEmpleado;
    }

    public void setEmpleadoRutEmpleado(Empleados empleadoRutEmpleado) {
        this.empleadosRutEmpleado = empleadoRutEmpleado;
    }

    public Proveedores getProveedoresRutProveedor() {
        return proveedoresRutProveedor;
    }

    public void setProveedoresRutProveedor(Proveedores proveedoresRutProveedor) {
        this.proveedoresRutProveedor = proveedoresRutProveedor;
    }

    @Override
    public String toString() {
        return "RegistroUsuario{" + "rutUsuario=" + rutUsuario + ", nombreUsuario=" + nombreUsuario + ", password=" + password + ", rolUsuario=" + rolUsuario + ", clientesRutCliente=" + clientesRutCliente + ", empleadoRutEmpleado=" + empleadosRutEmpleado + ", proveedoresRutProveedor=" + proveedoresRutProveedor + '}';
    }
        
}
