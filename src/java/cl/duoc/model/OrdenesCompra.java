/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class OrdenesCompra {

    private int numeroOrden;
    private String detalleOrden;
    private Clientes ClientesRutCliente;
    
    public OrdenesCompra() {
    }

    public OrdenesCompra(int numeroOrden, String detalleOrden, Clientes ClientesRutCliente) {
        this.numeroOrden = numeroOrden;
        this.detalleOrden = detalleOrden;
        this.ClientesRutCliente = ClientesRutCliente;
    }

    public int getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(int numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getDetalleOrden() {
        return detalleOrden;
    }

    public void setDetalleOrden(String detalleOrden) {
        this.detalleOrden = detalleOrden;
    }

    public Clientes getClientesRutCliente() {
        return ClientesRutCliente;
    }

    public void setClientesRutCliente(Clientes ClientesRutCliente) {
        this.ClientesRutCliente = ClientesRutCliente;
    }

    @Override
    public String toString() {
        return "OrdenesCompra{" + "numeroOrden=" + numeroOrden + ", detalleOrden=" + detalleOrden + ", ClientesRutCliente=" + ClientesRutCliente + '}';
    }
    

    
}
