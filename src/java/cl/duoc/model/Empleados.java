/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 *   Grupo 1
 */
public class Empleados {

    private String rutEmpleado;
    private String nombreEmpleado;
    private String apellidoEmpleado;
    private String cargoEmpleado;
    private String usuarioEmpleado;
    private String password;
    private Clientes clientesRutCliente;
    
    public Empleados() {
    }

    public Empleados(String rutEmpleado, String nombreEmpleado, String apellidoEmpleado, String cargoEmpleado, String usuarioEmpleado, String password, Clientes clientesRutCliente) {
        this.rutEmpleado = rutEmpleado;
        this.nombreEmpleado = nombreEmpleado;
        this.apellidoEmpleado = apellidoEmpleado;
        this.cargoEmpleado = cargoEmpleado;
        this.usuarioEmpleado = usuarioEmpleado;
        this.password = password;
        this.clientesRutCliente = clientesRutCliente;
    }

    public String getRutEmpleado() {
        return rutEmpleado;
    }

    public void setRutEmpleado(String rutEmpleado) {
        this.rutEmpleado = rutEmpleado;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getApellidoEmpleado() {
        return apellidoEmpleado;
    }

    public void setApellidoEmpleado(String apellidoEmpleado) {
        this.apellidoEmpleado = apellidoEmpleado;
    }

    public String getCargoEmpleado() {
        return cargoEmpleado;
    }

    public void setCargoEmpleado(String cargoEmpleado) {
        this.cargoEmpleado = cargoEmpleado;
    }

    public String getUsuarioEmpleado() {
        return usuarioEmpleado;
    }

    public void setUsuarioEmpleado(String usuarioEmpleado) {
        this.usuarioEmpleado = usuarioEmpleado;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Clientes getClientesRutCliente() {
        return clientesRutCliente;
    }

    public void setClientesRutCliente(Clientes clientesRutCliente) {
        this.clientesRutCliente = clientesRutCliente;
    }

    @Override
    public String toString() {
        return "Empleados{" + "rutEmpleado=" + rutEmpleado + ", nombreEmpleado=" + nombreEmpleado + ", apellidoEmpleado=" + apellidoEmpleado + ", cargoEmpleado=" + cargoEmpleado + ", usuarioEmpleado=" + usuarioEmpleado + ", password=" + password + ", clientesRutCliente=" + clientesRutCliente + '}';
    }

}
