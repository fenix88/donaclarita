/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Minutas {
    
    private int idMinuta;
    private String dia;    
    private String detalleDia;
    private ServicioComedor servicioComedorIdComedor; 

    public Minutas() {
    }

    public Minutas(int idMinuta, String dia, String detalleDia, ServicioComedor servicioComedorIdComedor) {
        this.idMinuta = idMinuta;
        this.dia = dia;
        this.detalleDia = detalleDia;
        this.servicioComedorIdComedor = servicioComedorIdComedor;
    }

    public int getIdMinuta() {
        return idMinuta;
    }

    public void setIdMinuta(int idMinuta) {
        this.idMinuta = idMinuta;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getDetalleDia() {
        return detalleDia;
    }

    public void setDetalleDia(String detalleDia) {
        this.detalleDia = detalleDia;
    }

    public ServicioComedor getServicioComedorIdComedor() {
        return servicioComedorIdComedor;
    }

    public void setServicioComedorIdComedor(ServicioComedor servicioComedorIdComedor) {
        this.servicioComedorIdComedor = servicioComedorIdComedor;
    }

    @Override
    public String toString() {
        return "Minutas{" + "idMinuta=" + idMinuta + ", dia=" + dia + ", detalleDia=" + detalleDia + ", servicioComedorIdComedor=" + servicioComedorIdComedor + '}';
    }
       
}
