    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Clientes {

    private String rutCliente;
    private String usuarioEmpresa;
    private String password;
    private String nombreCliente;
    private int telefono;
    private String direccion;
    
    
    public Clientes(String rutCliente, String usuarioEmpresa, String password, String nombreCliente, int telefono, String direccion) {
        this.rutCliente = rutCliente;
        this.usuarioEmpresa = usuarioEmpresa;
        this.password = password;
        this.nombreCliente = nombreCliente;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public Clientes() {
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getUsuarioEmpresa() {
        return usuarioEmpresa;
    }

    public void setUsuarioEmpresa(String usuarioEmpresa) {
        this.usuarioEmpresa = usuarioEmpresa;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Clientes{" + "rutCliente=" + rutCliente + ", usuarioEmpresa=" + usuarioEmpresa + ", password=" + password + ", nombreCliente=" + nombreCliente + ", telefono=" + telefono + ", direccion=" + direccion + '}';
    }

    
    
}
