/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.model;

/**
 *
 * @author Grupo 1
 */
public class Huespedes {

    public Huespedes() {
    }

    private String rutHuesped;     
    private String nombreHuesped;
    private String apellidoHuesped;
    private Habitaciones habitacionesIdHabitacion;
    private OrdenesCompra ordenesCompraNumeroOrden;  
    private Clientes clientesRutCliente;
    
    public Huespedes(String rutHuesped, String nombreHuesped, String apellidoHuesped, Habitaciones habitacionesIdHabitacion, OrdenesCompra ordenesCompraNumeroOrden, Clientes clientesRutCliente) {
        this.rutHuesped = rutHuesped;
        this.nombreHuesped = nombreHuesped;
        this.apellidoHuesped = apellidoHuesped;
        this.habitacionesIdHabitacion = habitacionesIdHabitacion;
        this.ordenesCompraNumeroOrden = ordenesCompraNumeroOrden;
        this.clientesRutCliente = clientesRutCliente;
    }

    public String getRutHuesped() {
        return rutHuesped;
    }

    public void setRutHuesped(String rutHuesped) {
        this.rutHuesped = rutHuesped;
    }

    public String getNombreHuesped() {
        return nombreHuesped;
    }

    public void setNombreHuesped(String nombreHuesped) {
        this.nombreHuesped = nombreHuesped;
    }

    public String getApellidoHuesped() {
        return apellidoHuesped;
    }

    public void setApellidoHuesped(String apellidoHuesped) {
        this.apellidoHuesped = apellidoHuesped;
    }

    public Habitaciones getHabitacionesIdHabitacion() {
        return habitacionesIdHabitacion;
    }

    public void setHabitacionesIdHabitacion(Habitaciones habitacionesIdHabitacion) {
        this.habitacionesIdHabitacion = habitacionesIdHabitacion;
    }

    public OrdenesCompra getOrdenesCompraNumeroOrden() {
        return ordenesCompraNumeroOrden;
    }

    public void setOrdenesCompraNumeroOrden(OrdenesCompra ordenesCompraNumeroOrden) {
        this.ordenesCompraNumeroOrden = ordenesCompraNumeroOrden;
    }

    public Clientes getClientesRutCliente() {
        return clientesRutCliente;
    }

    public void setClientesRutCliente(Clientes clientesRutCliente) {
        this.clientesRutCliente = clientesRutCliente;
    }

    @Override
    public String toString() {
        return "Huespedes{" + "rutHuesped=" + rutHuesped + ", nombreHuesped=" + nombreHuesped + ", apellidoHuesped=" + apellidoHuesped + ", habitacionesIdHabitacion=" + habitacionesIdHabitacion + ", ordenesCompraNumeroOrden=" + ordenesCompraNumeroOrden + ", clientesRutCliente=" + clientesRutCliente + '}';
    }
    

}
