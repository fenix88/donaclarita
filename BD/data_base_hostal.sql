-- ----------------------------------------------------------
-- Creacion de usuario y permisos
-- ----------------------------------------------------------

CREATE USER hostal_clarita IDENTIFIED BY "admin123";
ALTER USER hostal_clarita DEFAULT TABLESPACE users QUOTA UNLIMITED ON users;
ALTER USER hostal_clarita TEMPORARY TABLESPACE temp;
GRANT CREATE SESSION, CREATE VIEW, ALTER SESSION, CREATE SEQUENCE TO hostal_clarita;
GRANT CREATE SYNONYM, CREATE DATABASE LINK, RESOURCE, UNLIMITED TABLESPACE TO hostal_clarita;


-- ----------------------------------------------------------
-- Creacion de Tablas
-- ----------------------------------------------------------


CREATE TABLE clientes (
    rut_cliente      VARCHAR2(10 CHAR) NOT NULL,
    usuario_empresa  VARCHAR2(15 CHAR) NOT NULL,
    password         VARCHAR2(15 CHAR) NOT NULL,
    nombre_cliente   VARCHAR2(50) NULL,
    telefono         INTEGER NULL,
    direccion        VARCHAR2(50 CHAR) NULL
);

ALTER TABLE clientes ADD CONSTRAINT clientes_pk PRIMARY KEY ( rut_cliente );


CREATE TABLE empleados (
    rut_empleado          VARCHAR2(10 CHAR) NOT NULL,
    nombre_empleado       VARCHAR2(50 CHAR) NULL,
    apellido_empleado     VARCHAR2(50 CHAR) NULL,
    cargo_empleado        VARCHAR2(50 CHAR) NULL,
    usuario_empleado      VARCHAR2(10 CHAR) NOT NULL,
    password              VARCHAR2(15 CHAR) NOT NULL,
    clientes_rut_cliente  VARCHAR2(10 CHAR) NULL
);

ALTER TABLE empleados ADD CONSTRAINT empleados_pk PRIMARY KEY ( rut_empleado );


CREATE TABLE facturas (
    numero_factura        INTEGER NOT NULL,
    id_venta              INTEGER NOT NULL,
    precio_factura        INTEGER NOT NULL,
    clientes_rut_cliente  VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE facturas ADD CONSTRAINT facturas_pk PRIMARY KEY ( numero_factura );


CREATE TABLE habitaciones (
    id_habitacion        INTEGER NOT NULL,
    numero_habitacion    INTEGER NOT NULL,
    tipo_cama            VARCHAR2(15 CHAR) NOT NULL,
    accesorio            VARCHAR2(30 CHAR) NOT NULL,
    precio               INTEGER NOT NULL,
    cantidad_accesorios  INTEGER NOT NULL
);

ALTER TABLE habitaciones ADD CONSTRAINT habitaciones_pk PRIMARY KEY ( id_habitacion );


CREATE TABLE estado_habitacion (
    id_estado_habitacion        INTEGER NOT NULL,
    estado_habitacion           VARCHAR2(30) NOT NULL,
    habitaciones_id_habitacion  INTEGER NOT NULL
);

ALTER TABLE estado_habitacion ADD CONSTRAINT estado_habitacion_pk PRIMARY KEY ( id_estado_habitacion );


CREATE TABLE huespedes (
    rut_huesped                  VARCHAR2(10 CHAR) NOT NULL,
    nombre_huesped               VARCHAR2(50 CHAR) NOT NULL,
    apellido_huesped             VARCHAR2(50 CHAR) NOT NULL,
    habitaciones_id_habitacion   INTEGER NOT NULL,
    ordenes_compra_numero_orden  INTEGER NOT NULL,
    clientes_rut_cliente         VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE huespedes ADD CONSTRAINT huespedes_pk PRIMARY KEY ( rut_huesped );


CREATE TABLE minutas (
    id_minuta                    INTEGER NOT NULL,
    dia                          VARCHAR2(15 CHAR) NOT NULL,
    detalle_dia                  VARCHAR2(50 CHAR) NOT NULL,
    servicio_comedor_id_comedor  INTEGER NOT NULL
);

ALTER TABLE minutas ADD CONSTRAINT minutas_pk PRIMARY KEY ( id_minuta );


CREATE TABLE orden_pedido (
    id_pedido                  INTEGER NOT NULL,
    fecha_creacion             DATE NOT NULL,
    proveedores_rut_proveedor  VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE orden_pedido ADD CONSTRAINT orden_pedido_pk PRIMARY KEY ( id_pedido );


CREATE TABLE ordenes_compra (
    numero_orden          INTEGER NOT NULL,
    detalle_orden         VARCHAR2(50 CHAR) NOT NULL,
    clientes_rut_cliente  VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE ordenes_compra ADD CONSTRAINT ordenes_compra_pk PRIMARY KEY ( numero_orden );


CREATE TABLE platos (
    id_plato                     INTEGER NOT NULL,
    nombre_plato                 VARCHAR2(50 CHAR) NOT NULL,
    precio                       INTEGER NOT NULL,
    servicio_comedor_id_comedor  INTEGER NOT NULL,
    minutas_id_minuta            INTEGER NOT NULL
);

ALTER TABLE platos ADD CONSTRAINT platos_pk PRIMARY KEY ( id_plato );


CREATE TABLE productos (
    codigo_producto            VARCHAR2(50 CHAR) NOT NULL,
    nombre_producto            VARCHAR2(50 CHAR) NOT NULL,
    descripcion_producto       VARCHAR2(50 CHAR) NOT NULL,
    stock                      INTEGER NOT NULL,
    stock_critico              INTEGER NOT NULL,
    precio                     INTEGER NOT NULL,
    fecha_recepcion            DATE NOT NULL,
    empleados_rut_empleado    VARCHAR2(10 CHAR) NOT NULL,
    orden_pedido_id_pedido     INTEGER NOT NULL,
    proveedores_rut_proveedor  VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE productos ADD CONSTRAINT productos_pk PRIMARY KEY ( codigo_producto );


CREATE TABLE proveedores (
    rut_proveedor        VARCHAR2(10 CHAR) NOT NULL,
	usuario_proveedor    VARCHAR2(50 CHAR) NOT NULL,
	password             VARCHAR2(50 CHAR) NOT NULL,
	nombre_proveedor     VARCHAR2(50 CHAR) NULL,
    direccion_proveedor  VARCHAR2(50 CHAR) NULL,
    rubro_proveedor      VARCHAR2(50 CHAR) NULL,
    telefono             INTEGER NULL,
    correo_electronico   VARCHAR2(50 CHAR) NULL
);

ALTER TABLE proveedores ADD CONSTRAINT proveedores_pk PRIMARY KEY ( rut_proveedor );


CREATE TABLE servicio_comedor (
    id_comedor               INTEGER NOT NULL,
    tipo_servicio            VARCHAR2(50 CHAR) NOT NULL,
    facturas_numero_factura  INTEGER NOT NULL
);

ALTER TABLE servicio_comedor ADD CONSTRAINT servicio_comedor_pk PRIMARY KEY ( id_comedor );


CREATE TABLE registro_usuario (
    rut_usuario                VARCHAR2(10 CHAR) NOT NULL,
    nombre_usuario             VARCHAR2(15 CHAR) NOT NULL,
    password                   VARCHAR2(15 CHAR) NOT NULL,
    rol_usuario                VARCHAR2(15 CHAR) NOT NULL,
    clientes_rut_cliente       VARCHAR2(10 CHAR) NULL,
    empleados_rut_empleado    VARCHAR2(10 CHAR) NULL,
    proveedores_rut_proveedor  VARCHAR2(10 CHAR) NULL
);

ALTER TABLE registro_usuario ADD CONSTRAINT registro_usuario_pk PRIMARY KEY ( rut_usuario );

CREATE UNIQUE INDEX registro_usuario__idx ON
    registro_usuario (
        clientes_rut_cliente
    ASC );

CREATE UNIQUE INDEX registro_usuario__idxv1 ON
    registro_usuario (
        empleados_rut_empleado
    ASC );

CREATE UNIQUE INDEX registro_usuario__idxv2 ON
    registro_usuario (
        proveedores_rut_proveedor
    ASC );
	
			
-- ----------------------------------------------------------
-- Relaciones entre Tablas
-- ----------------------------------------------------------

ALTER TABLE empleados
    ADD CONSTRAINT empleados_clientes_fk FOREIGN KEY ( clientes_rut_cliente )
        REFERENCES clientes ( rut_cliente );

ALTER TABLE estado_habitacion
    ADD CONSTRAINT estado_room_habitaciones_fk FOREIGN KEY ( habitaciones_id_habitacion )
        REFERENCES habitaciones ( id_habitacion );

ALTER TABLE facturas
    ADD CONSTRAINT facturas_clientes_fk FOREIGN KEY ( clientes_rut_cliente )
        REFERENCES clientes ( rut_cliente );

ALTER TABLE huespedes
    ADD CONSTRAINT huespedes_clientes_fk FOREIGN KEY ( clientes_rut_cliente )
        REFERENCES clientes ( rut_cliente );

ALTER TABLE huespedes
    ADD CONSTRAINT huespedes_habitaciones_fk FOREIGN KEY ( habitaciones_id_habitacion )
        REFERENCES habitaciones ( id_habitacion );

ALTER TABLE huespedes
    ADD CONSTRAINT huespedes_ordenes_compra_fk FOREIGN KEY ( ordenes_compra_numero_orden )
        REFERENCES ordenes_compra ( numero_orden );

ALTER TABLE minutas
    ADD CONSTRAINT minutas_servicio_comedor_fk FOREIGN KEY ( servicio_comedor_id_comedor )
        REFERENCES servicio_comedor ( id_comedor );

ALTER TABLE orden_pedido
    ADD CONSTRAINT orden_pedido_proveedores_fk FOREIGN KEY ( proveedores_rut_proveedor )
        REFERENCES proveedores ( rut_proveedor );

ALTER TABLE ordenes_compra
    ADD CONSTRAINT ordenes_compra_clientes_fk FOREIGN KEY ( clientes_rut_cliente )
        REFERENCES clientes ( rut_cliente );

ALTER TABLE platos
    ADD CONSTRAINT platos_minutas_fk FOREIGN KEY ( minutas_id_minuta )
        REFERENCES minutas ( id_minuta );

ALTER TABLE platos
    ADD CONSTRAINT platos_servicio_comedor_fk FOREIGN KEY ( servicio_comedor_id_comedor )
        REFERENCES servicio_comedor ( id_comedor );

ALTER TABLE productos
    ADD CONSTRAINT productos_empleados_fk FOREIGN KEY ( empleados_rut_empleado )
        REFERENCES empleados ( rut_empleado );

ALTER TABLE productos
    ADD CONSTRAINT productos_orden_pedido_fk FOREIGN KEY ( orden_pedido_id_pedido )
        REFERENCES orden_pedido ( id_pedido );

ALTER TABLE productos
    ADD CONSTRAINT productos_proveedores_fk FOREIGN KEY ( proveedores_rut_proveedor )
        REFERENCES proveedores ( rut_proveedor );

ALTER TABLE registro_usuario
    ADD CONSTRAINT registro_user_proveedores_fk FOREIGN KEY ( proveedores_rut_proveedor )
        REFERENCES proveedores ( rut_proveedor );

ALTER TABLE registro_usuario
    ADD CONSTRAINT registro_usuario_clientes_fk FOREIGN KEY ( clientes_rut_cliente )
        REFERENCES clientes ( rut_cliente );

ALTER TABLE registro_usuario
    ADD CONSTRAINT registro_usuario_empleados_fk FOREIGN KEY ( empleados_rut_empleado )
        REFERENCES empleados ( rut_empleado );

ALTER TABLE servicio_comedor
    ADD CONSTRAINT servicio_comedor_facturas_fk FOREIGN KEY ( facturas_numero_factura )
        REFERENCES facturas ( numero_factura );
		
		
		
-- ----------------------------------------------------------
-- Poblamiento de Tablas
-- ----------------------------------------------------------




INSERT INTO clientes (rut_cliente,usuario_empresa,password,nombre_cliente,telefono,direccion)
VALUES ('30234757-2','logist','logz_476','Logistica Soto',994563212,'av maitenes 455, Quilicura, Santiago');
INSERT INTO clientes (rut_cliente,usuario_empresa,password,nombre_cliente,telefono,direccion)
VALUES ('20538452-1','consult','cnsy_624','Ramirez Consultores',984463179,'av tobalaba 133, Providencia, Santiago');
INSERT INTO clientes (rut_cliente,usuario_empresa,password,nombre_cliente,telefono,direccion)
VALUES ('15337251-6','carey','cry_971','Carey Abogados',976263472,'5 de abril 288, Maipu, Santiago');
INSERT INTO clientes (rut_cliente,usuario_empresa,password,nombre_cliente,telefono,direccion)
VALUES ('20577250-k','ingen','prz_938','Perez Ingenieria',947132999,'balmaceda 1245, Peñaflor, Santiago');

----------------------------------------------------------------------------------------------------------------
INSERT INTO empleados (rut_empleado,nombre_empleado,apellido_empleado,cargo_empleado,usuario_empleado,password,clientes_rut_cliente)
VALUES ('15343217-2','Fernando','Alvarez','Mantenedor de Productos','fe.alvarez','vafez_88','30234757-2');
INSERT INTO empleados (rut_empleado,nombre_empleado,apellido_empleado,cargo_empleado,usuario_empleado,password,clientes_rut_cliente)
VALUES ('14218925-5','Alberto','Sanchez','Mantenedor de Pedidos','al.sanchez','albCHZ_53','20577250-k');
INSERT INTO empleados (rut_empleado,nombre_empleado,apellido_empleado,cargo_empleado,usuario_empleado,password,clientes_rut_cliente)
VALUES ('19418722-k','Maria','Davila','Facturacion','ma.davila','mriDL_395','20577250-k');
INSERT INTO empleados (rut_empleado,nombre_empleado,apellido_empleado,cargo_empleado,usuario_empleado,password,clientes_rut_cliente)
VALUES ('17947122-8','Paola','Zuñiga','Recepcion Huespedes','pa.zuniga','zgaPL_731','20538452-1');
INSERT INTO empleados (rut_empleado,nombre_empleado,apellido_empleado,cargo_empleado,usuario_empleado,password,clientes_rut_cliente)
VALUES ('16143805-3','Carlos','Navarro','Gestion Proveedores','ca.navarro','nvrRC_306','15337251-6');

------------------------------------------------------------------------------------------------------------------
INSERT INTO facturas (numero_factura,id_venta,precio_factura,clientes_rut_cliente)
VALUES (000001,1,120000,'30234757-2');
INSERT INTO facturas (numero_factura,id_venta,precio_factura,clientes_rut_cliente)
VALUES (000002,2,250000,'20538452-1');
INSERT INTO facturas (numero_factura,id_venta,precio_factura,clientes_rut_cliente)
VALUES (000003,3,100000,'15337251-6');
INSERT INTO facturas (numero_factura,id_venta,precio_factura,clientes_rut_cliente)
VALUES (000004,4,250000,'20577250-k');

----------------------------------------------------------------------------------------------------------------
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (1,1,'individual','cafetera',25000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (2,2,'dual','TV cable',35000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (3,3,'individual','Insonorización',30000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (4,4,'individual','balcon',30000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (5,5,'cama litera','cama plegable',20000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (6,6,'dual','Frigobar',35000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (7,7,'dual','TV cable',35000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (8,8,'cama litera','TV cable',20000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (9,9,'premium','jacuzzi',40000,1);
INSERT INTO habitaciones (id_habitacion,numero_habitacion,tipo_cama,accesorio,precio,cantidad_accesorios)
VALUES (10,10,'premium','jacuzzi',40000,1);

---------------------------------------------------------------------------------------------------------------
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (1,'disponible',1);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (2,'disponible',2);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (3,'ocupada',3);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (4,'disponible',4);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (5,'ocupada',5);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (6,'disponible',6);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (7,'disponible',7);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (8,'ocupada',8);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (9,'disponible',9);
INSERT INTO estado_habitacion (id_estado_habitacion,estado_habitacion,habitaciones_id_habitacion)
VALUES (10,'ocupada',10);

------------------------------------------------------------------------------------------------------------
INSERT INTO ordenes_compra (numero_orden,detalle_orden,clientes_rut_cliente)
VALUES (1,'Pago via tarjeta VISA','30234757-2');
INSERT INTO ordenes_compra (numero_orden,detalle_orden,clientes_rut_cliente)
VALUES (2,'Pago en efectivo','20538452-1');
INSERT INTO ordenes_compra (numero_orden,detalle_orden,clientes_rut_cliente)
VALUES (3,'Pago en efectivo','15337251-6');
INSERT INTO ordenes_compra (numero_orden,detalle_orden,clientes_rut_cliente)
VALUES (4,'Pago via tarjeta VISA','20577250-k');

----------------------------------------------------------------------------------------------------------
INSERT INTO huespedes (rut_huesped,nombre_huesped,apellido_huesped,habitaciones_id_habitacion,ordenes_compra_numero_orden,clientes_rut_cliente)
VALUES ('13645865-2','Alfonso','Reyes',1,1,'20577250-k');
INSERT INTO huespedes (rut_huesped,nombre_huesped,apellido_huesped,habitaciones_id_habitacion,ordenes_compra_numero_orden,clientes_rut_cliente)
VALUES ('19656898-7','Juan','Alba',2,2,'20538452-1');
INSERT INTO huespedes (rut_huesped,nombre_huesped,apellido_huesped,habitaciones_id_habitacion,ordenes_compra_numero_orden,clientes_rut_cliente)
VALUES ('17837895-5','Maria','Arenas',3,3,'15337251-6');
INSERT INTO huespedes (rut_huesped,nombre_huesped,apellido_huesped,habitaciones_id_habitacion,ordenes_compra_numero_orden,clientes_rut_cliente)
VALUES ('15894271-k','Pedro','Martinez',4,4,'30234757-2');

----------------------------------------------------------------------------------------------------------------
INSERT INTO servicio_comedor (id_comedor,tipo_servicio,facturas_numero_factura)
VALUES (1,'General',000001);
INSERT INTO servicio_comedor (id_comedor,tipo_servicio,facturas_numero_factura)
VALUES (2,'Especial',000002);
INSERT INTO servicio_comedor (id_comedor,tipo_servicio,facturas_numero_factura)
VALUES (3,'Ejecutivo',000003);
INSERT INTO servicio_comedor (id_comedor,tipo_servicio,facturas_numero_factura)
VALUES (4,'Buffet',000004);

----------------------------------------------------------------------------------------------------
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (1,'Lunes','Caldillo,charqui,filete,filete mignon,lomo',1);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (2,'Martes','bife,escalopa,milanesa,suprema ave',1);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (3,'Miercoles','chuleta,lengua normal y vacuno,congrio,merluza',2);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (4,'Jueves','salmon,jaiba,caldillo,paila marina,ensalada',2);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (5,'Viernes','hamburguesa,pastel choclo y camarones,papas',3);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (6,'Sabado','pure natural,ensalada surtida,tortilla verduras',3);
INSERT INTO minutas (id_minuta,dia,detalle_dia,servicio_comedor_id_comedor)
VALUES (7,'Domingo','ensalada palta y frutas,torta casera,panqueques',4);

-----------------------------------------------------------------------------------------------------------------
INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,nombre_proveedor,direccion_proveedor,rubro_proveedor,telefono,correo_electronico)
VALUES ('26834965-6','carn.r','crn_867','Carniceraias Raquel','Los eucaliptus 4523,Maipu,Santiago','carnes y pescados',935476287,'raquel_carnes@gmail.com');
INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,nombre_proveedor,direccion_proveedor,rubro_proveedor,telefono,correo_electronico)
VALUES ('40223766-3','frut.j','frt_867','Fruteria Juanita','errazuriz 356,Cerrillos,Santiago','frutas y verduras',988734652,'juanita.fruts77@gmail.com');
INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,nombre_proveedor,direccion_proveedor,rubro_proveedor,telefono,correo_electronico)
VALUES ('56230876-4','bebe.c','bbc_934','Bebestibles central','av.central,Independencia,Santiago','Bebidas y jugos',966517346,'central_bebidas89@gmail.com');
INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,nombre_proveedor,direccion_proveedor,rubro_proveedor,telefono,correo_electronico)
VALUES ('15898459-3','nut.r','ntr_5768','Nutrisa S.A','Andes 45,Maipu,Santiago','Azucares y Harina',932170582,'nutrisa_alimentos@gmail.com');
INSERT INTO proveedores (rut_proveedor,usuario_proveedor,password,nombre_proveedor,direccion_proveedor,rubro_proveedor,telefono,correo_electronico)
VALUES ('40003334-k','agro.x','agr_687','Agroprodex','av.jaime guzman,Renca,Santiago','legumbres y pastas',9455789,'contacto@agroprodex.cl');


------------------------------------------------------------------------------------------------------------------------

INSERT INTO orden_pedido (id_pedido,fecha_creacion,proveedores_rut_proveedor)
VALUES (1,sysdate,'26834965-6');
INSERT INTO orden_pedido (id_pedido,fecha_creacion,proveedores_rut_proveedor)
VALUES (2,sysdate,'40223766-3');
INSERT INTO orden_pedido (id_pedido,fecha_creacion,proveedores_rut_proveedor)
VALUES (3,sysdate,'15898459-3');
INSERT INTO orden_pedido (id_pedido,fecha_creacion,proveedores_rut_proveedor)
VALUES (4,sysdate,'40003334-k');

------------------------------------------------------------------------------------------------------------------
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (1,'Caldillo de congrio',4500,1,1);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (2,'Charquicán',3500,1,1);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (3,'Filete',4500,1,1);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (4,'Filete Mignon',6000,1,1);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (5,'Lomo Vetado',6200,1,1);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (6,'Bife de Chorizo',5700,1,2);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (7,'Escalopa Kaiser',7800,1,2);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (8,'Milanesa de Vacuno',6300,1,2);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (9,'Suprema de Ave',4900,1,2);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (10,'Milanesa de Ave',4000,1,2);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (11,'Chuleta de Cerdo',6100,2,3);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (12,'Lengua de Vacuno',3500,2,3);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (13,'Lengua Boloñesa',3500,2,3);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (14,'Congrio',4100,2,3);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (15,'Merluza',5500,2,3);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (16,'Salmon',6500,2,4);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (17,'Chupe de jaibas',7800,2,4);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (18,'Caldillo de Congrio',8500,2,4);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (19,'Paila Marina',7000,2,4);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (20,'Ensalada Pollo',6500,2,4);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (21,'Hamburguesa Vegetariana',3500,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (22,'Pastel de Choclo',3900,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (23,'Pastel de Camarones',3900,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (24,'Papas Duquesas',3000,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (25,'Papas a la chilena',3500,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (26,'Arroz',2300,3,5);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (27,'Puré natural',2500,3,6);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (28,'Ensalada surtida',2000,3,6);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (29,'Tortilla de verduras',2200,3,6);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (30,'Ensalada de palta',2100,4,7);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (31,'Torta casera',2000,4,7);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (32,'Panqueques',1800,4,7);
INSERT INTO platos (id_plato,nombre_plato,precio,servicio_comedor_id_comedor,minutas_id_minuta)
VALUES (33,'Ensalada de frutas',2700,4,7);

-----------------------------------------------------------------------------------------------------------
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000001','1/2kg de salame','salame metro rebanado 500gr',17,15,6000,sysdate,'15343217-2',1,'26834965-6');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000002','1kg de tomate','1kg de tomate a granel',55,40,800,sysdate,'15343217-2',2,'40223766-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000003','1kg de palta','1kg depalta hass',40,20,2300,sysdate,'15343217-2',2,'40223766-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000004','1kg de arroz','1kg de arroz basmati integral',12,10,3000,sysdate,'15343217-2',4,'40003334-k');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000005','1kg de limon','bolsa de limon 1kg',7,5,500,sysdate,'15343217-2',2,'40223766-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000006','1kg de harina','1kg de harina con polvos para hornear',15,10,1000,sysdate,'15343217-2',3,'15898459-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000007','1kg de papas','1kg de papas sureñas',15,10,1800,sysdate,'15343217-2',4,'40003334-k');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000008','1kg de filete','filete reineta fresca',11,10,5000,sysdate,'15343217-2',1,'26834965-6');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000009','1kg de azucar','1kg azucar blanca granulada',15,10,1350,sysdate,'15343217-2',3,'15898459-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000010','1kg de choclo','bolsa 1kg choclo',7,5,3000,sysdate,'15343217-2',2,'40223766-3');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000011','1kg de pollo','1kg pechuga deshuesada de pollo',20,25,3790,sysdate,'15343217-2',1,'26834965-6');
INSERT INTO productos (codigo_producto,nombre_producto,descripcion_producto,stock,stock_critico,precio,fecha_recepcion,empleados_rut_empleado,orden_pedido_id_pedido,proveedores_rut_proveedor)
VALUES ('00000000000000012','1kg de jaiba','1kg carne de jaiba desmenuzada',6,5,23500,sysdate,'15343217-2',1,'26834965-6');





