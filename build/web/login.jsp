<%-- 
    Document   : Principal
    Created on : 04-05-2020, 14:09:25
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <title>TODO supply a title</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <body>
        <nav class="navbar" style="background-color: #333333">
            <div class="form-inline">
                <a class="nav-link" href="index.jsp" style="color: white"><span class="navbar-toggler-icon"></span> Inicio </a>
                <a class="nav-link" href="contacto.jsp" style="color: white"><span class="navbar-toggler-icon"></span> Contacto </a>
            </div>
            <div class="text-center" style="color: white">
                <h3><strong>Hostal Doña Clarita</strong></h3>
            </div>

            <a style="color: white" href="login.jsp" class="nav-link">Iniciar Session</a>
        </nav>
        <div class="container col-lg-3" >
            <form id="frm-login" action="login-servlet" method="POST">
                <div class="form-group text-center">
                    <img src="img/login.png" height="160" width="160" >
                    <p><strong>Inicie Sesión</strong></p>
                </div>
                <div class="form-group">
                    <label>Usuario</label>
                    <input class="form-control" type="text" name="txt_user" placeholder="Ingrese su nombre de usuario" maxlength="20">
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input class="form-control" type="password" name="txt_pass" placeholder="Ingrese su contraseña" maxlength="20">
                </div>
                <div class="row">
                    <c:if test="${not empty sessionScope.error}">
                        <div id="msg-error" class="form-group col-lg-12 btn btn-danger" >
                            ${sessionScope.error}
                        </div>
                    </c:if>
                </div>
                <div class="dropdown-divider" ></div>
                <input class="btn btn-block" type="submit" name="btn_action" value="Iniciar Sesion" style="color: white; background-color: #71b100; "> 
                <br>
                <div class="form-group" style="text-align: center;">¿No tienes Cuenta?
                    <a href="registrar.jsp" > Registrar </a>
                </div>

            </form>
        </div>

    </body>
</html>
