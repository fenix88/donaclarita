<%-- 
    Document   : contaco
    Created on : 09-07-2020, 18:58:07
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar" style="background-color: #333333">
            <div class="form-inline">
                <a class="nav-link" href="index.jsp" style="color: white"><span class="navbar-toggler-icon"></span> Inicio </a>
                <a class="nav-link" href="contacto.jsp" style="color: white"><span class="navbar-toggler-icon"></span> Contacto </a>
            </div>
            <div class="text-center" style="color: white">
                <h3><strong>Hostal Doña Clarita</strong></h3>
            </div>

            <a style="color: white" href="login.jsp" class="nav-link">Iniciar Session</a>
        </nav>
        <br>
        <br>
        <h2></h2>

        <div class="rowCon" >

            <div class="media"  >
                <div class="media-left">
                    <br>
                    <br>
                    <img src="img/contacto.jpg" alt="" width="400px;"/>
                </div>
                <div class="media-body">
                    <h3 class="media-heading">CONTÁCTANOS</h3>
                    </br>
                    <i>
                        <p> Si tienes dudas, sugerencias o reclamos, por favor contáctanos a través de los siguientes medios.</p>
                    </i>
                    </br>

                    <h4 class="icon fa-mobile"><span class="">Telefono</span></h4>
                    <p>  22-5438965 </p>
                    <p>  9 57835265 </p>
                    
                    <h4 class="icon fa-home"><span class="">Dirección</span></h4>
                    <p>  1234 Avenida Jose Miguel Carrera</br>
                        Rancagua</br>
                        Chile</br>
                    </p>
                    

                    <h4 class="icon fa-envelope-o"><span class="">Correo electronico</span></h4>
                    <a href="#">donaclarita@donaclarita.com</a>
                    </li>
                </div>
            </div>

        </div>
    </body>
</html>
