<%-- 
    Document   : Registrar.jsp
    Created on : 13-05-2020, 2:32:15
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="container col-lg-4" >
            <form action="registrar-usuario" method="POST">
                <div class="form-group text-center">
                    <h3><strong>Registro de Usuario</strong></h3>
                </div>
                <br>
                <div class="form-group">
                    <label for="exampleInputEmail1">Ingrese su Rut</label>
                    <input type="text" class="form-control" id="rut" name="txt_reg_rut" placeholder="XXXXXXXX-X" maxlength="10">
                    <small id="emailHelp" class="form-text text-muted">Ingrese su rut sin puntos.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Ingrese un Nombre de Usuario</label>
                    <input type="text" class="form-control" id="user_name" name="txt_reg_user" placeholder="XXXXXXX" maxlength="15" >
                    <small id="emailHelp" class="form-text text-muted">Ingrese un nombre de usuario Valido</small>
                </div> 
                <div class="form-group">
                    <label for="exampleInputEmail1">Ingrese una Contraseña</label>
                    <input type="password" class="form-control" name="txt_reg_pass" id="password" >
                    <small id="emailHelp" class="form-text text-muted">Ingrese una contraseña de minimo 8 caracteres, debe incluir mayusculas, minusculas y numeros.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Seleccione Rol Usuario</label>
                    <select class="form-control" name="sl_rol">
                        <option selected>Rol Usuario</option>
                        <option>Cliente</option>
                        <option>Empleado</option>
                        <option>Proveedor</option>
                    </select>  
                    <small id="emailHelp" class="form-text text-muted">Seleccione el rol de usuario con el que se registrara</small>
                </div>

                <input class="btn btn-primary btn-lg btn-block" type="submit" name="btn_registrar_usuario" value="Registrarse" style="background-color: #333333"> 
                <br>

            </form>
        </div>

   
    </body>
</html>
