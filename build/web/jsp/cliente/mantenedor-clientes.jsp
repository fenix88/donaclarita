<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
        <h1>Modulo Cliente</h1>
        <div class="container mt-4" >

            <form> 
                <br>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="colFormLabel" placeholder="" disabled>
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre de Usuario</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="colFormLabel" placeholder="" disabled>
                    </div>
                </div>

                <br>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">
                        <h6><span class="col-lg-2 control-label">Password</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="colFormLabel" placeholder="" disabled >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Completo</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="colFormLabel" placeholder="Ingrese su nombre completo"  >
                    </div>
                </div>
                
                <br>
                
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">
                        <h6><span class="col-lg-2 control-label">Telefono</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="colFormLabel" maxlength="9"  placeholder="ingrese su numero 9XXXXXXXX" >
                    </div>                   
                </div>
                
                <br>
                
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">
                        <h6><span class="col-lg-2 control-label">Direccion</span></h6>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="colFormLabel" maxlength="50"  placeholder="ingrese su direccion" >
                    </div>                   
                </div>


                <div class="form-group ">
                        <button type="button" class="btn" style="background-color: #f0b810; color: white;">Modificar</button>
                        <button type="button" class="btn" style="background-color: #f44336; color: white;" >Eliminar</button>
                </div>


                    
                </div>
            </form>
        </div>
    </div>
</body>
</html>