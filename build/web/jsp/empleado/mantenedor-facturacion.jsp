<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            

        <div class="container mt-4" >
            <h1>Modulo Facturacion</h1>
            <br>
            <form action="${pageContext.request.contextPath}/factura-empleado-servlet" method="POST">

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Numero Factura</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="rut" name="txt_numero_factura" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Precio Factura</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="nombre-cliente" name="txt_precio_factura" placeholder="" >
                    </div>

                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Cliente</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cboCliente">
                            <option value=""> Selecionar Cliente</option>
                            <c:forEach var="cliente" items="${lista}" varStatus="i"> 
                                <option value="${cliente.clientesRutCliente.rutCliente}">${cliente.clientesRutCliente.rutCliente}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">ID Venta</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="id-venta" name="txt_id_venta" placeholder="" >
                    </div> 
                </div>
                <br>
                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="submit" class="btn btn-success">Agregar Factura</button>
                    </div>
                </div>

            </form>
        </div>
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Numero Factura</th>
                    <th>Precio Factura</th>
                    <th>Id Venta</th>
                    <th>Rut Cliente</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <c:forEach items="${lista}" var="p" varStatus="i" >
                <tbody> 
                    <tr>
                        <td><input type="text" id="txt-numero-factura-${i.count}" value="${p.numeroFactura}" name="codigo" disabled="true" style="font-size: 15px !important"/></td>
                        <td><input type="text" id="txt-precio-factura-${i.count}" value="${p.precioFactura}" disabled="true" style="font-size: 15px !important"/></td>
                        <td><input type="text" id="txt-id-venta-${i.count}" value="${p.idVenta}" disabled="true" style="font-size: 15px !important"/></td>
                        <td><input type="text" id="txt-rut-cliente-${i.count}" value="${p.clientesRutCliente.rutCliente}" disabled="true" style="font-size: 15px !important"/></td>
                        <td>
                            <div class="form-inline">
                                <button id="bnt-edit-${i.count}" type="button" onclick="edit('${i.count}')" class="btn btn-warning btn-sm">Modificar</button> 
                                <button id="bnt-delete-${i.count}" type="button" onclick="deleteRow('${i.count}')" class="btn btn-danger btn-sm ">Eliminar</button>  
                                <button id="bnt-save-${i.count}" type="button" onclick="saveRow('${i.count}')" class="btn btn-success btn-sm" hidden="true">Guardar</button>
                                <button id="bnt-cancel-${i.count}" type="button" onclick="cancelRow('${i.count}')" class="btn btn-danger btn-sm" hidden="true">CANCEL</button>
                            </div>
                        </td>

                    </tr>
                </tbody>
            </c:forEach>
        </table>
    </section>


</body>
<script>

    function edit(row) {


        editRow(row, false, false, false, false, true, true,
                false, false);
    }

    function cancelRow(row) {
        editRow(row, true, true, true, true, false, false,
                true, true);
    }

    function editRow(row, numeroFacturaDisabled, precioFacturaDisabled,
            idVentasDisabled, nombreClienteDisabled,
            editDisabled, deleteDisabled, saveDisabled, cancelDisabled) {

        var txtNumeroFactura = "txt-numero-factura-" + row;
        var txtPrecioFactura = "txt-precio-factura-" + row;
        var txtIdVenta = "txt-id-venta-" + row;
        var txtRutCliente = "txt-rut-cliente-" + row;
        var btnEdit = 'bnt-edit-' + row;
        var btnDelete = 'bnt-delete-' + row;
        var btnSave = 'bnt-save-' + row;
        var btnCancel = 'bnt-cancel-' + row;

        document.getElementById(txtNumeroFactura).disabled = numeroFacturaDisabled;
        document.getElementById(txtPrecioFactura).disabled = precioFacturaDisabled;
        document.getElementById(txtIdVenta).disabled = idVentasDisabled;
        document.getElementById(txtRutCliente).disabled = nombreClienteDisabled;
        document.getElementById(btnEdit).hidden = editDisabled;
        document.getElementById(btnDelete).hidden = deleteDisabled;
        document.getElementById(btnSave).hidden = saveDisabled;
        document.getElementById(btnCancel).hidden = cancelDisabled;
    }

    function deleteRow(row) {
        var id = document.getElementById("txt-numero-factura-" + row).value;
        cancelRow(row);

        $.ajax({
            url: '${pageContext.request.contextPath}/delete-factura-servlet?codigo=' + id,
            method: 'POST',
            success: window.location.reload()
        });

    }

    function saveRow(row) {

        var numero_factura = document.getElementById("txt-numero-factura-" + row).value;
        var precio_factura = document.getElementById("txt-precio-factura-" + row).value;
        var rut_cliente = document.getElementById("txt-rut-cliente-" + row).value;
        var nombre_cliente = document.getElementById("txt-nombre-cliente-" + row).value;


        cancelRow(row);
                const Http = new XMLHttpRequest();
                const url = '${pageContext.request.contextPath}/UpdateProductoServlet?numero_factura=' + numero_factura
                +'&precio_factura=' + precio_factura
                + '&rut_cliente=' + rut_cliente + '&nombre_cliente=' + nombre_cliente;
        Http.open('POST', url);
        Http.send();
    }

</script>
</html>
