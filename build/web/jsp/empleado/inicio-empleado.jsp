<%-- 
    Document   : Inicio_Empleado
    Created on : 23-05-2020, 4:59:29
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
    </head>
    <body>        
        <jsp:include page="../template/navbar.jsp"/>        

        <div class="container">

            <div class="row row-cols-1 row-cols-md-3">
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/cliente_empleado.png" class="card-img-top img-fluid" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Mi perfil</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/jsp/empleado/mantenedor-empleado.jsp" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/mantenedor_huesped.png" class="card-img-top img-fluid" alt="Card image cap" >
                        <div class="card-body">
                            <h5 class="card-title">Mantenedor Huesped</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/huespedes-servlet" class="btn btn-outline-primary  btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/mantenedor_proveedor.png" class="card-img-top img-fluid" alt="Card image cap" style="background-color: #f5f5f5;">
                        <div class="card-body">
                            <h5 class="card-title">Mantenedor Proveedor</h5>
                            <p class="card-text"></p>
                           <a href="${pageContext.request.contextPath}/proveedores-servlet" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/factura(1).jpg" class="card-img-top img-fluid" alt="Card image cap" style="">
                        <div class="card-body">
                            <h5 class="card-title">Modulo Facturacion</h5>
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/factura-empleado-servlet" class="btn btn-outline-primary btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/productos.png" class="card-img-top" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Mantenedor Productos</h5>                             
                            <p class="card-text"></p>
                            <a href="${pageContext.request.contextPath}/producto-servlet" class="btn btn-outline-primary  btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <img src="${pageContext.request.contextPath}/img/estadisticas.jpg" class="card-img-top" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Mantenedor Orden Pedido</h5>
                            <p class="card-text"></p>
                            <a href="mantenedor-orden-pedido.jsp" class="btn btn-outline-primary  btn-block" style="text-align: center" > Entrar </a>
                        </div>
                    </div>
                </div>
                        
            </div>

        </div>


    </body>
</html>
