<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            

        <div class="container mt-4" >
            <h1>Modulo Huesped</h1>
            <br>
            <form action="${pageContext.request.contextPath}/huespedes-servlet" method="POST">
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-rut-huesped" name="txt_rut" placeholder="">
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-nombre-huesped" name="txt_nombre" placeholder="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Apellido Huesped</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txt-apellido-huesped" name="txt_apellido" placeholder="">
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">N° de Habitacion</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cbo-numero-habitacion">
                            <option value=""> Selecionar N° de Habitacion</option>
                            <c:forEach var="habitacion" items="${lista}" varStatus="i"> 
                                <option value="${habitacion.habitacionesIdHabitacion.idHabitacion}">${habitacion.habitacionesIdHabitacion.idHabitacion}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">N° Orden</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cbo-orden-pedido">
                            <option value=""> Selecionar N° de Orden</option>
                            <c:forEach var="orden" items="${lista}" varStatus="i"> 
                                <option value="${orden.ordenesCompraNumeroOrden.numeroOrden}">${orden.ordenesCompraNumeroOrden.numeroOrden}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Cliente</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cbo-rut-cliente">
                            <option value=""> Selecionar Rut de Cliente</option>
                            <c:forEach var="cliente" items="${lista}" varStatus="i"> 
                                <option value="${cliente.clientesRutCliente.rutCliente}">${cliente.clientesRutCliente.rutCliente}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="submit" class="btn btn-success" name="btn_agregar_huesped" value="Agrgera_huesped">Agregar</button>
                    </div>
                </div>
            </form>

        </div>

        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Rut Huesped</th>
                    <th>Nombre Huesped</th>
                    <th>Apellido Huesped</th>
                    <th>Numero Habitacion</th>
                    <th>Numero de Orden</th>
                    <th>Rut Cliente</th>                
                </tr>
            </thead>
            <c:forEach items="${lista}" var="p" varStatus="i" >
                <tbody> 
                    <tr>
                        <td><input type="text" name="rut-huesped" id="txt-rut-huesped-${i.count}" name="rut" value="${p.rutHuesped}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" name="nombre-huesped" id="txt-nombre-huesped-${i.count}" value="${p.nombreHuesped}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" name="apellido-huesped" id="txt-apellido-huesped-${i.count}" value="${p.apellidoHuesped}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" name="numero-habitacion" id="txt-numero-habitacion-${i.count}" value="${p.habitacionesIdHabitacion.idHabitacion}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" name="numero-orden" id="txt-numero-orden-${i.count}" value="${p.ordenesCompraNumeroOrden.numeroOrden}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" name="rut-cliente" id="txt-rut-cliente-${i.count}" value="${p.clientesRutCliente.rutCliente}" disabled="true" style="font-size: 10px !important"/></td>
                        <td>
                            <button id="bnt-edit-${i.count}" type="button" onclick="edit('${i.count}')" class="btn btn-warning btn-sm">Modificar</button> 
                            <button id="bnt-delete-${i.count}" type="button" onclick="deleteRow('${i.count}')" class="btn btn-danger btn-sm ">Eliminar</button>  
                            <button id="bnt-save-${i.count}" type="button" onclick="saveRow('${i.count}')" class="btn btn-success col-sm-5" hidden="true">Guardar</button>
                            <button id="bnt-cancel-${i.count}" type="button" onclick="cancelRow('${i.count}')" class="btn btn-danger col-sm-5" hidden="true">CANCEL</button> 
                        </td>
                    </tr>
                </tbody>
            </c:forEach>
        </table>  
    </body>
    
    <script>

    function edit(row) {
        editRow(row, false, false,false, false,false, false, true, true,
                false, false);
    }
    
    function cancelRow(row) {
            editRow(row, true, true,true, true,true, true, false, false,
                    true, true);
        }

    function editRow(row, rutHuespedDisabled, nombreHuespedDisabled, 
                        apellidoHuespedDisabled, numeroHabitacionDisabled, numeroOrdenDisabled, rutClienteDisabled,
                        editDisabled, deleteDisabled, saveDisabled, cancelDisabled) {

                        
        var txtRutHuesped = "txt-rut-huesped-" + row;
        var txtNombreHuesped = "txt-nombre-huesped-" + row;
        var txtApellidoHuesped = "txt-apellido-huesped-" + row;
        var txtNumeroHabitacion = "txt-numero-habitacion-" + row;
        var txtNumeroOrden = "txt-numero-orden-" + row;
        var txtRutCliente = "txt-rut-cliente-" + row;
        var btnEdit = 'bnt-edit-' + row;
        var btnDelete = 'bnt-delete-' + row;
        var btnSave = 'bnt-save-' + row;
        var btnCancel = 'bnt-cancel-' + row;

        document.getElementById(txtRutHuesped).disabled = rutHuespedDisabled;
        document.getElementById(txtNombreHuesped).disabled = nombreHuespedDisabled;
        document.getElementById(txtApellidoHuesped).disabled = apellidoHuespedDisabled;
        document.getElementById(txtNumeroHabitacion).disabled = numeroHabitacionDisabled;
        document.getElementById(txtNumeroOrden).disabled = numeroOrdenDisabled;
        document.getElementById(txtRutCliente).disabled = rutClienteDisabled;
        document.getElementById(btnEdit).hidden = editDisabled;
        document.getElementById(btnDelete).hidden = deleteDisabled;
        document.getElementById(btnSave).hidden = saveDisabled;
        document.getElementById(btnCancel).hidden = cancelDisabled;
    }
    
    function deleteRow(row) {
        var id = document.getElementById("txt-rut-huesped-" + row).value;
        cancelRow(row);

        $.ajax({
            url: '${pageContext.request.contextPath}/delete-huesped-servlet?rut=' + id,
            method: 'POST',
            success: window.location.reload()
        });

        }

</script>
    
</html>
