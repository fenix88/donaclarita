<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>  
        <div class="container mt-4" >
            <h1>Mantenedor Proveedor</h1>

            <form action="${pageContext.request.contextPath}/proveedores-servlet" method="POST">
                <br>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Proveedor</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="rut-proveedor" name="txt_rut_proveedor" placeholder="" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Usuario</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="usuario-proveedor" name="txt_nombre_usuario" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Contraseña</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="password-proveedor" name="txt_pass" placeholder="" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Proveedor</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="nombre-proveedor" name="txt_nombre_provedor" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rubro</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="rubro" name="txt_rubro" placeholder="" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Direccion</span></h6>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="direccion" name="txt_direccion" placeholder="" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Telefono</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="telefono" name="txt_telefono" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Correo Electronico</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" id="telefono" name="txt_correo_proveedor" placeholder="" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="submit" class="btn btn-success" name="btnAgregar">Agregar</button>
                    </div>
                </div>
            </form>
        </div>
        <section>
            <br>
            <h3 style="text-align: center;">Listado Proveedores</h3>

            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Rut Proveedor</th>
                        <th>Usuario Proveedor</th>
                        <th>Password</th>
                        <th>Nombre Proveedor</th>
                        <th>Direccion </th>
                        <th>Rubro</th>
                        <th>Telefono</th>
                        <th>Correo Electronico</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <c:forEach items="${lista}" var="p" varStatus="i" >
                    <tbody> 
                        <tr>
                            <td><input type="text" id="txt-rut-proveedor-${i.count}" value="${p.rutProveedor}" name="codigo" disabled="true" style="font-size: 10px !important" /></td>
                            <td><input type="text" id="txt-usuario-proveedor-${i.count}" value="${p.usuarioProveedor}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-password-proveedor-${i.count}" value="${p.password}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-nombre-proveedor-${i.count}" value="${p.nombreProveedor}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-direccion-proveedor-${i.count}" value="${p.direccionProveedor}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-rubro-proveedor-${i.count}" value="${p.rubroProveedor}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-telefono-proveedor-${i.count}" value="${p.telefono}" disabled="true" style="font-size: 10px !important"/></td>
                            <td><input type="text" id="txt-correo-proveedor-${i.count}" value="${p.correoElectronico}" disabled="true" style="font-size: 10px !important"/></td>
                            <td>
                                <div class="form-inline">
                                    <button id="bnt-edit-${i.count}" type="button" onclick="edit('${i.count}')" class="btn btn-warning btn-sm">Modificar</button> 
                                    <button id="bnt-delete-${i.count}" type="button" onclick="deleteRow('${i.count}')" class="btn btn-danger btn-sm ">Eliminar</button>  
                                    <button id="bnt-save-${i.count}" type="button" onclick="saveRow('${i.count}')" class="btn btn-success btn-sm" hidden="true">Guardar</button>
                                    <button id="bnt-cancel-${i.count}" type="button" onclick="cancelRow('${i.count}')" class="btn btn-danger btn-sm" hidden="true">CANCEL</button>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                </c:forEach>
            </table>
        </section>
    </body>
    <script>

        function edit(row) {


            editRow(row, false, false, false, false, false, false, false, false, true, true,
                    false, false);
        }

        function cancelRow(row) {
            editRow(row, true, true, true, true, true, true, true, true, false, false,
                    true, true);
        }

        function editRow(row, rutProveedorDisabled, usuarioProveedorDisabled,
                passwordProveedorDisabled, nombreProveedorDisabled, direccionProveedorDisabled, rubroProveedorDisabled,
                telefonoProveedorDisabled, correoProveedorDisbled, 
                editDisabled, deleteDisabled, saveDisabled, cancelDisabled) {


            var txtRutProveedor = "txt-rut-proveedor-" + row;
            var txtUsuarioProveedor = "txt-usuario-proveedor-" + row;
            var txtPasswordProveedor = "txt-password-proveedor-" + row;
            var txtNombreProveedor = "txt-nombre-proveedor-" + row;
            var txtDireccionProveedor = "txt-direccion-proveedor-" + row;
            var txtRubroProveedor = "txt-rubro-proveedor-" + row;
            var txtTelefonoProveedor = "txt-telefono-proveedor-" + row;
            var txtCorreoProveedor = "txt-correo-proveedor-" + row;
            var btnEdit = 'bnt-edit-' + row;
            var btnDelete = 'bnt-delete-' + row;
            var btnSave = 'bnt-save-' + row;
            var btnCancel = 'bnt-cancel-' + row;

            document.getElementById(txtRutProveedor).disabled = rutProveedorDisabled;
            document.getElementById(txtUsuarioProveedor).disabled = usuarioProveedorDisabled;
            document.getElementById(txtPasswordProveedor).disabled = passwordProveedorDisabled;
            document.getElementById(txtNombreProveedor).disabled = nombreProveedorDisabled;
            document.getElementById(txtDireccionProveedor).disabled = direccionProveedorDisabled;
            document.getElementById(txtRubroProveedor).disabled = rubroProveedorDisabled;
            document.getElementById(txtTelefonoProveedor).disabled = telefonoProveedorDisabled;
            document.getElementById(txtCorreoProveedor).disabled = correoProveedorDisbled;
            document.getElementById(btnEdit).hidden = editDisabled;
            document.getElementById(btnDelete).hidden = deleteDisabled;
            document.getElementById(btnSave).hidden = saveDisabled;
            document.getElementById(btnCancel).hidden = cancelDisabled;
        }

        function deleteRow(row) {
            var id = document.getElementById("txt-rut-proveedor-" + row).value;
            cancelRow(row);

            $.ajax({
                url: '${pageContext.request.contextPath}/delete-proveedor-servlet?codigo=' + id,
                method: 'POST',
                success: window.location.reload()
            });

        }
        
        function saveRow(row) {

            var codigo_producto = document.getElementById("txt-codigo-producto-" + row).value;
            var nombre_producto = document.getElementById("txt-nombre-producto-" + row).value;
            var descripcion = document.getElementById("txt-descripcion-producto-" + row).value;
            var stock = document.getElementById("txt-stock-" + row).value;
            var stock_critico = document.getElementById("txt-stock-critico-" + row).value;
            var precio = document.getElementById("txt-precio-" + row).value;
            var fecha_recepcion = document.getElementById("txt-fecha-recepcion-" + row).value;
            var empleados_rut_empleado = document.getElementById("txt-empleado-rut-empleado-" + row).value;
            var orden_pedido_id_pedido = document.getElementById("txt-orden-pedido-id-pedido-" + row).value;
            var proveedores_rut_proveedor = document.getElementById("txt-proveedor-rut-proveedor-" + row).value;
            
            cancelRow(row);
            const Http = new XMLHttpRequest();
            const url = '${pageContext.request.contextPath}/UpdateProductoServlet?codigo_producto=' + codigo_producto 
                    + '&nombre=' + nombre_producto
                    + '&descripcion=' + descripcion + '&stock=' + stock
                    + '&stock_critico=' + stock_critico + '&precio=' + precio
                    + '&fecha_recepcion=' + fecha_recepcion + '&empleados_rut_empleado=' + empleados_rut_empleado
                    + '&orden_pedido_id_pedido=' + orden_pedido_id_pedido
                    + '&proveedores_rut_proveedor=' + proveedores_rut_proveedor;
            Http.open('POST', url);
            Http.send();
        }

    </script>     
    
    
    
    
    
</html>
