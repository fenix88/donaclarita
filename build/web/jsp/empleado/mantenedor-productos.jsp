<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
        <div class="container" >
            <h1>Mantenedor Productos</h1>  
            <br>    

            <form  action="${pageContext.request.contextPath}/producto-servlet" method="POST">

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Codigo Producto</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="codigo-producto" name="txt_codigo" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Nombre Producto</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="nombre-producto" name="txt_nombre" placeholder="" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Descripcion Producto</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="descripcion-producto" name="txt_descripcion" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Stock</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="stock" name="txt_stock" placeholder="" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Stock Critico</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="stock-critico" name="txt_stock_critico" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Precio</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="precio" name="txt_precio" placeholder="" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Fecha Recepcion</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <input type="date" class="form-control" id="fecha-recepcion" value="2020-06-05" name="txt_fecha" placeholder="" >
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Empleado</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cboEmpleado">
                            <option value=""> Selecionar Rut Empleado</option>
                            <c:forEach var="empleado" items="${lista}" varStatus="i"> 
                                <option value="${empleado.empleadosRutEmpleado.rutEmpleado}">${empleado.empleadosRutEmpleado.rutEmpleado}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Orden de Pedido</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cboOrden">
                            <option value=""> Selecionar Orden de Pedido</option>
                            <c:forEach var="orden" items="${lista}" varStatus="i"> 
                                <option value="${orden.ordenPedidoIdPedido.idPedido}">${orden.ordenPedidoIdPedido.idPedido}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <h6><span class="col-lg-2 control-label">Rut Proveedor</span></h6>
                    </label>
                    <div class="col-sm-3">
                        <select name="cboProveedor">
                            <option value=""> Selecionar Rut Proveedor</option>
                            <c:forEach var="proveedor" items="${lista}" varStatus="i"> 
                                <option value="${proveedor.proveedoresRutProveedor.rutProveedor}">${proveedor.proveedoresRutProveedor.rutProveedor}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>



                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <button type="submit" class="btn btn-success" name="btn_agregar_producto" value="Agrgera_producto">Agregar</button> 
                    </div>
                </div>

            </form>
        </div> 
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Stock</th>
                    <th>Stock Critico</th>
                    <th>Precio</th>
                    <th>Fecha Recepecion</th>
                    <th>Empleado Asociado</th>
                    <th>Orden Pedido</th>
                    <th>Proveedor Asociado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <c:forEach items="${lista}" var="p" varStatus="i" >
                <tbody> 
                    <tr>
                        <td><input type="text" id="txt-codigo-producto-${i.count}" name="codigo" value="${p.codigoProducto}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-nombre-producto-${i.count}" value="${p.nombreProducto}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-descripcion-producto-${i.count}" value="${p.descripcionProducto}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-stock-${i.count}" value="${p.stock}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-stock-critico-${i.count}" value="${p.stockCritico}"  disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-precio-${i.count}" value="${p.precio}" disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-fecha-recepcion-${i.count}" value="${p.fechaRecepcion}" disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-empleado-rut-empleado-${i.count}" value="${p.empleadosRutEmpleado.rutEmpleado}" disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-orden-pedido-id-pedido-${i.count}" value="${p.ordenPedidoIdPedido.idPedido}" disabled="true" style="font-size: 10px !important"/></td>
                        <td><input type="text" id="txt-proveedor-rut-proveedor-${i.count}" value="${p.proveedoresRutProveedor.rutProveedor}" disabled="true" style="font-size: 10px !important"/></td>
                        <td>
                            <button id="bnt-edit-${i.count}" type="button" onclick="edit('${i.count}')" class="btn btn-warning btn-sm">Modificar</button> 
                            <button id="bnt-delete-${i.count}" type="button" onclick="deleteRow('${i.count}')" class="btn btn-danger btn-sm ">Eliminar</button>  
                            <button id="bnt-save-${i.count}" type="button" onclick="saveRow('${i.count}')" class="btn btn-success col-sm-5" hidden="true">Guardar</button>
                            <button id="bnt-cancel-${i.count}" type="button" onclick="cancelRow('${i.count}')" class="btn btn-danger col-sm-5" hidden="true">CANCEL</button> 
                        </td>
                    </tr>
                </tbody>
            </c:forEach>
        </table>  


    </body>
    <script>

        function edit(row) {


            editRow(row, false, false, false, false, false, false, false, false, false, true, true,
                    false, false);
        }

        function cancelRow(row) {
            editRow(row, true, true, true, true, true, true, true, true, true, false, false,
                    true, true);
        }

        function editRow(row, nombreProductoDisabled, descripcionProductoDisabled,
                stockDisabled, stockCriticoDisabled, precioDisabled, fechaRecepcionDisabled,
                empleadoRutEmpleadoDisabled, ordenPedidoIdPedidoDisbled, proveedorRutProveedorDisabled,
                editDisabled, deleteDisabled, saveDisabled, cancelDisabled) {


            var txtNombreProducto = "txt-nombre-producto-" + row;
            var txtDescripcionProducto = "txt-descripcion-producto-" + row;
            var txtStock = "txt-stock-" + row;
            var txtStockCritico = "txt-stock-critico-" + row;
            var txtPrecio = "txt-precio-" + row;
            var txtFechaRecepcion = "txt-fecha-recepcion-" + row;
            var txtEmpleadoRutEmpleado = "txt-empleado-rut-empleado-" + row;
            var txtOrdenPedidoIdPedido = "txt-orden-pedido-id-pedido-" + row;
            var txtProveedorRutProveedor = "txt-proveedor-rut-proveedor-" + row;
            var btnEdit = 'bnt-edit-' + row;
            var btnDelete = 'bnt-delete-' + row;
            var btnSave = 'bnt-save-' + row;
            var btnCancel = 'bnt-cancel-' + row;

            document.getElementById(txtNombreProducto).disabled = nombreProductoDisabled;
            document.getElementById(txtDescripcionProducto).disabled = descripcionProductoDisabled;
            document.getElementById(txtStock).disabled = stockDisabled;
            document.getElementById(txtStockCritico).disabled = stockCriticoDisabled;
            document.getElementById(txtPrecio).disabled = precioDisabled;
            document.getElementById(txtFechaRecepcion).disabled = fechaRecepcionDisabled;
            document.getElementById(txtEmpleadoRutEmpleado).disabled = empleadoRutEmpleadoDisabled;
            document.getElementById(txtOrdenPedidoIdPedido).disabled = ordenPedidoIdPedidoDisbled;
            document.getElementById(txtProveedorRutProveedor).disabled = proveedorRutProveedorDisabled;
            document.getElementById(btnEdit).hidden = editDisabled;
            document.getElementById(btnDelete).hidden = deleteDisabled;
            document.getElementById(btnSave).hidden = saveDisabled;
            document.getElementById(btnCancel).hidden = cancelDisabled;
        }

        function deleteRow(row) {
            var id = document.getElementById("txt-codigo-producto-" + row).value;
            cancelRow(row);

            $.ajax({
                url: '${pageContext.request.contextPath}/DeleteProductoServlet?codigo=' + id,
                method: 'POST',
                success: window.location.reload()
            });

        }
        
        function saveRow(row) {

            var codigo_producto = document.getElementById("txt-codigo-producto-" + row).value;
            var nombre_producto = document.getElementById("txt-nombre-producto-" + row).value;
            var descripcion = document.getElementById("txt-descripcion-producto-" + row).value;
            var stock = document.getElementById("txt-stock-" + row).value;
            var stock_critico = document.getElementById("txt-stock-critico-" + row).value;
            var precio = document.getElementById("txt-precio-" + row).value;
            var fecha_recepcion = document.getElementById("txt-fecha-recepcion-" + row).value;
            var empleados_rut_empleado = document.getElementById("txt-empleado-rut-empleado-" + row).value;
            var orden_pedido_id_pedido = document.getElementById("txt-orden-pedido-id-pedido-" + row).value;
            var proveedores_rut_proveedor = document.getElementById("txt-proveedor-rut-proveedor-" + row).value;
            
            cancelRow(row);
            const Http = new XMLHttpRequest();
            const url = '${pageContext.request.contextPath}/UpdateProductoServlet?codigo_producto=' + codigo_producto 
                    + '&nombre=' + nombre_producto
                    + '&descripcion=' + descripcion + '&stock=' + stock
                    + '&stock_critico=' + stock_critico + '&precio=' + precio
                    + '&fecha_recepcion=' + fecha_recepcion + '&empleados_rut_empleado=' + empleados_rut_empleado
                    + '&orden_pedido_id_pedido=' + orden_pedido_id_pedido
                    + '&proveedores_rut_proveedor=' + proveedores_rut_proveedor;
            Http.open('POST', url);
            Http.send();
        }

    </script>
</html>
