<%-- 
    Document   : Inicio_Cliente
    Created on : 18-05-2020, 5:11:06
    Author     : Brian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../template/header.jsp"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>       
        <jsp:include page="../template/navbar.jsp"/>            
        
        <div class="container-fluid" >
            <h1>Modulo Orden de Pedido</h1>
            <div class="row row-cols-1 row-cols-md-4">
            <form class="form-inline">
               <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Usuario</span></h6>
                        <input type="text" class="form-control"  placeholder="" disabled 
                value="<c:if test="${not empty sessionScope.user}">
                    ${sessionScope.user}
                </c:if>" id="usuario">
                    </div>
                </div>
                
               <div class="form-group">  
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">N° Orden</span></h6>
                        <input type="text" class="form-control" placeholder="" id="numero-orden">
                    </div>
                </div>

               <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Rut Proveedor</span></h6>
                        <input type="text" class="form-control" placeholder="" id="rut-proveedor">
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Nombre Producto</span></h6>
                        <input type="text" class="form-control" placeholder="" id="nombre-producto">
                    </div>
               </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Codigo Producto</span></h6>
                        <input type="text" class="form-control" placeholder="" id="codigo-producto">
                    </div>
               </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Cantidad</span></h6>
                        <input type="text" class="form-control" placeholder="" id="cantidad">
                    </div>
               </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Descripcion</span></h6>
                        <input type="text" class="form-control" placeholder="" id="descripcion">
                    </div>
               </div>

                <div class="form-group row">
                    <h6><span class="col-lg-2 control-label">Fecha Recepcion</span></h6>
                    <input class="form-control" type="datetime-local" value="2020-06-05" id="datetime">
                </div>

                <div class="form-group">
                    <div class="form-group mx-sm-3">
                        <h6><span class="col-lg-2 control-label">Precio</span></h6>
                        <input type="text" class="form-control" placeholder="" id="precio">
                    </div>
               </div>

               <div class="form-group">
                    <div class="form-group mx-sm-3">
                            <button type="button" class="btn btn-success">Agregar Orden</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </body>
</html>
